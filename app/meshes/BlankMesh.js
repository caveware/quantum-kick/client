/**
 * @author Josef Frank
 * @class BlankMesh
 * @desc Blank mesh for positioning other children meshes
 */
export default class BlankMesh extends BABYLON.Mesh {
	/**
	 * @constructor
	 * @param {String} name Name of mesh
	 * @param {BABYLON.Scene} scene Scene to add mesh to
	 */
	constructor (name, scene) {
		super(name, scene)

		this.visible = false
	}

	/**
	 * Get x position
	 * @returns {Number} x position
	 */
	get x () { return this.position.x }

	/**
	 * Get y position
	 * @returns {Number} y position
	 */
	get y () { return this.position.y }

	/**
	 * Get z position
	 * @returns {Number} z position
	 */
	get z () { return this.position.z }

	/**
	 * Set x position
	 * @param {Number} x x position
	 */
	set x (x) { this.position.x = x }

	/**
	 * Set y position
	 * @param {Number} y y position
	 */
	set y (y) { this.position.y = y }

	/**
	 * Set z position
	 * @param {Number} z z position
	 */
	set z (z) { this.position.z = z }
}
