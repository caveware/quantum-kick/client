import BlankMesh from "./BlankMesh"

export default class MultiMesh extends BlankMesh {
	constructor (name, scene, meshes) {
		super(name, scene)
		this.name = name
		this.scene = scene
		this.meshes = meshes
		meshes[0].setParent(this)
		meshes.forEach((mesh) => {
			mesh.position.z = 0
		})
	}

	clone () {
		return new MultiMesh(this.name + "_clone", this.scene, [this.meshes[0].clone()])
	}

	dispose () {
		this.meshes.forEach((mesh) => {
			this.scene.removeMesh(mesh)
			mesh.dispose()
		})
		this.scene.removeMesh(this)
		super.dispose()
	}

	get show () {
		return this.visibility
	}

	set show (value) {
		this.visibility = value
		this.meshes.forEach((mesh) => {
			mesh.visibility = value
		})
	}
}