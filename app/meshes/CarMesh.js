// Classes
import AntiEnergyParticles from "../particles/AntiEnergyParticles"
import AssetsManager from "../managers/AssetsManager"
import BlankMesh from "./BlankMesh"

// Constants
import { BODY_MESHES, WHEEL_MESHES } from "../constants/car"

/**
 * @author Josef Frank
 * @class CarMesh
 * @desc Renderer for the car
 */
export default class CarMesh extends BlankMesh {
	/**
	 * @constructor
	 * @param {String} name Name of the car mesh
	 * @param {BABYLON.scene} scene Scene to add mesh to
	 * @param {Object} config Configuration options
	 */
	constructor (name, scene, config) {
		super(name, scene)

		// Set internal variables
		this._frontRotation = 0
		this._juice = 0
		this._scene = scene
		this._hidden = false
		this._meshes = {}
		this.body = config.body
		this.wheel = config.wheel

		// Create sound
		// this.snd_rev = new BABYLON.Sound(`${name}_REV`, "assets/sounds/car/rev.ogg", scene, null, {
		// 	autoplay: true,
		// 	distanceModel: "exponential",
		// 	loop: true,
		// 	maxDistance: 50,
		// 	spatialSound: true,
		// })

		// Setup beams
		this._beam = BABYLON.MeshBuilder.CreateCylinder(name + "_beam", {
			diameterBottom: 4,
			diameterTop: .25,
			height: 42,
		}, scene)
		this._beam.setParent(this)
		this._beam.position.set(0, -21, 0)
		const beam_material = new BABYLON.StandardMaterial()
		beam_material.alpha = .25
		beam_material.disableLighting = true
		beam_material.emissiveColor.set(1, 1, 1)
		this._beam.material = beam_material
		this._beam.outlineWidth = .5
		this._beam.renderOutline = true
	}

	setupParticles () {
		this.aeParticles = new AntiEnergyParticles(this.name + "_p", this._scene, this)
	}

	/**
	 * Cool
	 * @param {Object} ui UI
	 */
	createComboBox (ui) {
		this._ui = ui

		this._comboBox = new BABYLON.GUI.Rectangle()
		this._comboBox.background = "transparent"
		this._comboBox.color = "transparent"
		this._comboBox.height = "128px"
		this._comboBox.width = "212px"
		this._ui.addControl(this._comboBox)

		this._comboText = new BABYLON.GUI.TextBlock()
		this._comboText.color = "white"
		this._comboText.fontSize = "16px"
		this._comboText.height = "24px"
		this._comboText.text = "3000 x 2"
		this._comboText.top = "54px"
		this._comboText.width = "100%"
		this._comboText.shadowBlur = 8
		this._comboBox.addControl(this._comboText)

		this._pool = []
		for (var i = 0; i < 50; i++) {
			const text = new BABYLON.GUI.TextBlock()
			text.color = "white"
			text.fontSize = "12px"
			text.height = "24px"
			text.left = `${-40 + 80 * Math.random()}px`
			text.text = "OK"
			text.top = "72px"
			text.shadowBlur = 8
			this._pool.push(text)
			this._comboBox.addControl(text)
		}

		this._comboBox.linkWithMesh(this._meshes.body)
		this._comboBox.linkOffsetY = -116
	}

	/**
	 * Creates the name tag for the player.
	 * @param {*} ui UI element to add tag to.
	 * @returns {*} The created tag.
	*/
	createTag (ui) {
		this._ui = ui

		this._tag = new BABYLON.GUI.Rectangle()
		this._tag.background = "Black"
		this._tag.color = "Red"
		this._tag.height = "24px"
		this._tag.width = "96px"

		this._tag_text = new BABYLON.GUI.TextBlock()
		this._tag_text.color = "White"
		this._tag_text.fontSize = "16em"
		this._tag_text.text = this.name
		this._tag.addControl(this._tag_text)

		this._tag_juice = new BABYLON.GUI.Rectangle()
		this._tag_juice.background = "Red"
		this._tag_juice.color = "Black"
		this._tag_juice.height = "4px"
		this._tag_juice.width = "100%"
		this._tag_juice.top = "10px"
		this._tag.addControl(this._tag_juice)

		this._tag_juice.horizontalAlignment = 0

		this._ui.addControl(this._tag)

		this._tag.linkWithMesh(this._meshes.body)
		this._tag.linkOffsetY = -40

		return this._tag
	}

	/**
	 * Create UI
	 * @param {Object} ui UI
	 * @param {Number} key index
	 */
	createUI (ui, key) {
		const odd = key % 2 !== 0

		const box = new BABYLON.GUI.Rectangle()
		box.background = "transparent"
		box.color = "transparent"
		box.width = "350px"
		box.height = "160px"
		const margin = "80px"

		if (odd) {
			box.left = "-80px"
			box.horizontalAlignment = 1
		} else {
			box.left = margin
			box.horizontalAlignment = 0
		}

		if (key < 2) {
			box.top = margin
			box.verticalAlignment = 0
		} else {
			box.top = "-80px"
			box.verticalAlignment = 1
		}

		// Player name
		const name = new BABYLON.GUI.TextBlock()
		name.color = odd ? "Cyan" : "Red"
		name.fontSize = "26em"
		name.horizontalAlignment = 0
		name.verticalAlignment = 0
		name.textHorizontalAlignment = 0
		name.textVerticalAlignment = 0
		name.text = this.name
		name.shadowBlur = 2
		box.addControl(name)

		const createControl = (key, button, texts, y) => {
			const butt = new BABYLON.GUI.Image(key + "_btn", `assets/controls/xbox/${button}.png`)
			butt.stretch = BABYLON.GUI.Image.STRETCH_NONE
			butt.autoScale = false
			butt.width = butt.height = "32px"
			butt.horizontalAlignment = butt.verticalAlignment = 0
			butt.top = `${40 * y}px`
			box.addControl(butt)

			const text = this[key] = new BABYLON.GUI.TextBlock()
			text.color = "black"
			text.text = texts
			text.fontSize = "24em"
			text.left = "40px"
			text.top = butt.top
			text.horizontalAlignment = text.verticalAlignment = 0
			text.textHorizontalAlignment = text.textVerticalAlignment = 0
			box.addControl(text)
		}

		createControl("ULTIMATE_ANTI_ENERGY", "b", "ANTI-ENERGY", 1)
		createControl("ULTIMATE_PULSE", "x", "PULSE", 2)
		createControl("ULTIMATE_OVERCHARGE", "y", "OVERCHARGE", 3)

		ui.addControl(box)
	}

	/**
	 * Disposes of this mesh and children
	 */
	dispose () {
		// Dispose of children
		if (this._meshes.body) this._meshes.body.dispose()
		if (this._meshes.wheels) this._meshes.wheels.forEach(w => w.dispose())

		// Dispose of self
		super.dispose()
	}

	// --------
	// GETTERS / SETTERS
	// --------

	/**
	 * Getter for body
	 * @member body
	 */
	get body () {
		return this._body
	}

	/**
	 * Setter for body
	 * @member body
	 * @param {String} new_body New ID of body
	 */
	set body (new_body) {
		if (new_body !== this._body) {
			// Remove mesh from scene if currently have one added
			if (this._meshes.body) this._scene.removeMesh(this._meshes.body)

			// Load new body
			this._body = new_body

			// Determine body mesh
			const body_mesh = this._getBodyMeshByID(new_body)

			const loader = new AssetsManager(this._scene)
			loader.addMeshTask(body_mesh.id, body_mesh.mesh, (data) => {
				this._meshes.body = data.loadedMeshes[0]

				// Set mesh parent and position mesh correctly
				this._meshes.body.setParent(this)
				this._meshes.body.position.set(0, 0, 0)

				this._positionWheels()

				/// Update hidden or not
				this.hidden = this.hidden
			})
			loader.load()
		}
	}

	/**
	 * Getter for hidden
	 * @member hidden
	 */
	get hidden () {
		return this._hidden
	}

	/**
	 * Setter for hidden
	 * @member hidden
	 * @param {Boolean} hidden Whether or not to hide
	 */
	set hidden (hidden) {
		this._hidden = hidden
		const visible = !hidden

		// Set mesh visibilities
		if (this._meshes.body) this._meshes.body.isVisible = visible
		if (this._meshes.wheels) {
			this._meshes.wheels.forEach(wheel => { wheel.isVisible = visible })
		}
	}

	/**
	 * Getter for wheel
	 * @member wheel
	 */
	get wheel () {
		return this._wheel
	}

	/**
	 * Setter for wheel
	 * @member wheel
	 * @param {String} new_wheel New ID of wheel
	 */
	set wheel (new_wheel) {
		return
		if (new_wheel !== this._wheel) {
			// Remove mesh from scene if currently have one added
			if (this._meshes.wheels) {
				this._meshes.wheels.forEach(this._scene.removeMesh)
			}

			// Load new wheel
			this._wheel = new_wheel

			// Determine wheel mesh
			const wheel_mesh = WHEEL_MESHES.find((wheel) => {
				return wheel.id === this._wheel
			})

			const loader = new AssetsManager(this._scene)
			loader.addMeshTask(wheel_mesh.id, wheel_mesh.mesh, (data) => {
				const wheel = data.loadedMeshes[0]
				this._meshes.wheels = []

				// Set mesh parent and position mesh correctly
				for (let i = 0; i < 4; i++) {
					const w = wheel.clone()
					w.setParent(this)
					this._meshes.wheels.push(w)
				}

				// Position the wheels correctly
				this._positionWheels()

				/// Update hidden or not
				this.hidden = this.hidden

				// Remove original mesh
				this._scene.removeMesh(wheel)
			})
			loader.load()
		}
	}

	/**
	 * Finds body mesh by id.
	 * @param {String} id ID of body mesh
	 * @private
	 * @returns {*} Body mesh
	 */
	_getBodyMeshByID (id) {
		return BODY_MESHES.find(body => (body.id === id))
	}

	/**
	 * Repositions the wheels.
	 */
	_positionWheels () {
		if (!this._meshes.wheels) return

		// Determines wheel positions
		const d = this._getBodyMeshByID(this.body).wheels
		const d1 = d[0]
		const d2 = d[1]

		// Attach wheels to axels
		this._meshes.wheels[0].position.set(d1.x, .25, d1.z)
		this._meshes.wheels[1].position.set(-d1.x, .25, d1.z)
		this._meshes.wheels[2].position.set(d2.x, .25, d2.z)
		this._meshes.wheels[3].position.set(-d2.x, .25, d2.z)

		// Rotate wheels on side
		this._updateRotations()
	}

	/**
	 * Sets up the rotation.
	 */
	_updateRotations () {
		this._meshes.wheels[0].rotation.y = Math.PI + this._frontRotation
		this._meshes.wheels[1].rotation.y = this._frontRotation
		this._meshes.wheels[2].rotation.y = Math.PI
	}

	/**
	 * Sets the rotation of the front wheels.
	 * @param {Number} value Front rotation value
	 */
	setFrontRotation (value) {
		this._frontRotation = value
		this._updateRotations()
	}

	/**
	 * Rotates the wheels by value.
	 * @param {Number} value Rotation amount
	 */
	spinWheels (value) {
		return
		this._meshes.wheels.forEach((wheel, key) => {
			wheel.rotation.x += (key % 2 === 0) ? -value : value
		})
	}

	/**
	 * Updates the car mesh
	 * @param {Object} data Contains update data
	 * @param {Number} dt Delta time
	 */
	update (data, dt) {
		this._meshes.body.rotation.x = .05
		this._meshes.body.rotation.y += 3 * dt
		const is_red = this.alignment === "RED"

		// this.snd_rev.attachToMesh(this)
		const dist_x = Math.pow(this.x - data.position.x, 2)
		const dist_z = Math.pow(this.z - data.position.z, 2)
		const dist = Math.sqrt(dist_x + dist_z)

		if (data.events.used_powerup) Sound.playSFX("car_boost")

		this.spinWheels(dist / 10)

		this.x = data.position.x
		this.y = data.position.y
		this.z = data.position.z

		this.alignment = data.alignment
		if (this._tag) {
			const col = is_red ? "Red" : "Aqua"
			this._tag.color = col
			this._tag_juice.background = col
			this._tag.linkWithMesh(this._meshes.body)
			this._tag_juice.width = `${data.juice}%`
			this._tag.zIndex = Math.floor(this._tag.topInPixels)
			this._comboBox.linkWithMesh(this._meshes.body)
			this._comboBox.zIndex = Math.floor(this._tag.topInPixels)
		}

		const r = data.rotation
		this.rotation.set(r.x, -(r.y + Math.PI / 2) - Math.PI / 2, r.z)

		// Update beam
		const colour = new BABYLON.Color3(is_red ? 1 : 0, is_red ? 0 : 1, is_red ? 0 : 1)
		this._beam.outlineColor = colour
		this._beam.outlineWidth = .25 + Math.random() / 4
		this._beam.material.emissiveColor = colour
	}

	/**
	 * Scores
	 * @param {Object} scores Scores
	 */
	updateScores (scores) {
		const comboY = `${72 - Math.min(1, scores.combo_timeout * 2) * 20}px`
		this._comboText.text = `${scores.combo_base} x ${scores.combo_multiplier}`
		this._comboText.top = comboY

		const floats = scores.scores.slice(-50).map((f) => {
			return f.data
		})
		this._pool.forEach((txt, index) => {
			const f = floats[index]
			if (f) {
				txt.left = "0px"
				txt.text = `+${f.value}`
				txt.top = `${72 - 100 * f.progress}px`
			} else {
				txt.text = ""
			}
		})
	}

	/**
	 * updateui
	 * @param {Object} data data
	 */
	updateUI (data) {
		const keys = [ "ULTIMATE_ANTI_ENERGY", "ULTIMATE_PULSE", "ULTIMATE_OVERCHARGE" ]
		const names = [ "ANTI-ENERGY", "PULSE", "OVERCHARGE" ]

		keys.forEach((key, index) => {
			const text = this[key]
			const pos = data.ultimates.indexOf(key)
			const i = pos === -1 ? 2 : pos
			const charge = Math.floor(data.ultimate_charge[i] * 100)

			const ready = (charge < 100)
				? ` (${charge}%)`
				: " (ready)"

			text.text = names[index] + ready
		})
	}
}
