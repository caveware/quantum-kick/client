// Constants
import config from "../../assets/particles/anti_energy_particle/particles.json"

// Libraries
import ParticleController from "./ParticleController"

/**
 * @author Josef Frank
 * @class BubbleParticles
 * @desc Controller for bubble particles.
 */
export default class AntiEnergyParticles extends ParticleController {
	/**
	 * @constructor
	 * @param {String} name Name of bubble particles instance
	 * @param {BABYLON.Scene} scene Scene to add bubble particles to
	 * @param {*} parent Parent to attach bubble particles to
	 */
	constructor (name, scene, parent) {
		super(name, scene, parent, config)
	}
}
