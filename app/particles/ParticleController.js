/**
 * @author Josef Frank
 * @class ParticleController
 * @desc Base controller for particles
 */
export default class ParticleController {
	/**
	 * @constructor
	 * @param {String} name Name of bubble particles instance
	 * @param {BABYLON.Scene} scene Scene to add bubble particles to
	 * @param {*} parent Parent to attach bubble particles to
	 * @param {Object} [config] Configuration for particles
	 */
	constructor (name, scene, parent, config) {
		this._name = name
		this._parent = parent
		this._scene = scene
		if (config) this.applyConfig(config)
	}

	/**
	 * Applies a configuration to the particle controller
	 * @param {Object} data Configuration to apply
	 */
	applyConfig (data) {
		this._config = data

		if (this._particles) {
			this._particles.stop()
			this._particles.dispose()
		}

		// Set up emitter
		const particles = new BABYLON.ParticleSystem(this._name, data.maxParticles, this._scene, null, data.textureAnimates)
		particles.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD
		particles.disposeOnStop = true
		particles.emitter = this._parent

		// Particle images
		const image = `./assets/textures/particles/${data.image}`
		particles.particleTexture = new BABYLON.Texture(image, this._scene, true,
			false, BABYLON.Texture.TRILINEAR_SAMPLINGMODE)
		particles.textureMask = new BABYLON.Color4(...data.textureMask)

		// Animation settings
		particles.startSpriteCellID = data.startSpriteCellID
		particles.endSpriteCellID = data.endSpriteCellID
		particles.spriteCellHeight = data.spriteCellHeight
		particles.spriteCellWidth = data.spriteCellWidth
		particles.spriteCellLoop = data.spriteCellLoop
		particles.spriteCellChangeSpeed = data.spriteCellChangeSpeed

		// Color settings
		particles.color1 = new BABYLON.Color4(...data.color1)
		particles.color2 = new BABYLON.Color4(...data.color2)
		particles.colorDead = new BABYLON.Color4(...data.colorDead)

		// Direction settings
		particles.direction1 = new BABYLON.Vector3(...data.direction1)
		particles.direction2 = new BABYLON.Vector3(...data.direction2)

		// Rotational settings
		particles.maxAngularSpeed = data.maxAngularSpeed
		particles.minAngularSpeed = data.minAngularSpeed

		// Emit settings
		particles.emitRate = data.emitRate
		particles.maxEmitBox = new BABYLON.Vector3(...data.maxEmitBox)
		particles.minEmitBox = new BABYLON.Vector3(...data.minEmitBox)
		particles.maxEmitPower = data.maxEmitPower
		particles.minEmitPower = data.minEmitPower

		// Life time
		particles.maxLifeTime = data.maxLifeTime
		particles.minLifeTime = data.minLifeTime

		// Size
		particles.maxSize = data.maxSize
		particles.minSize = data.minSize

		// General settings
		particles.gravity = new BABYLON.Vector3(...data.gravity)
		particles.manualEmitCount = data.emitCount
		particles.targetStopDuration = data.targetStopDuration
		particles.updateSpeed = data.updateSpeed

		this._particles = particles
	}

	/**
	 * Destroys the particle controller
	 */
	dispose () {
		this.stop()
		this._particles.dispose()
		delete this
	}

	/**
	 * Start the particle controller
	 */
	start () {
		if (!this._particles) {
			throw new Error("Particle controller not configured")
		}

		this._particles.start()
	}

	/**
	 * Stop the particle controller
	 */
	stop () {
		if (!this._particles) {
			throw new Error("Particle controller not configured")
		}

		this._particles.stop()
	}
}
