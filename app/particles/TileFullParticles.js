// Constants
import config from "../../assets/particles/tile_stop/particles.json"

// Libraries
import ParticleController from "./ParticleController"

/**
 * @author Josef Frank
 * @class BubbleParticles
 * @desc Controller for bubble particles.
 */
export default class TileFullParticles extends ParticleController {
	/**
	 * @constructor
	 * @param {String} name Name of bubble particles instance
	 * @param {BABYLON.Scene} scene Scene to add bubble particles to
	 * @param {*} parent Parent to attach bubble particles to
	 */
	constructor (name, scene, parent) {
		super(name, scene, parent, config)
	}
}
