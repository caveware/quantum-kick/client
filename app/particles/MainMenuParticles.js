/**
 * Main Menu Particles
 * @module MainMenuParticles
 */
export default class MainMenuParticles {
	/**
	 * @constructor
	 * @param {BABYLON.Scene} scene Scene to add self to
	 */
	constructor (scene) {
		this.scene = scene

		this.fountain = BABYLON.Mesh.CreateBox("foutain", 1.0, scene)
		this.fountain.position.y = 10
		// Create a particle system
		var particleSystem = window.yu = new BABYLON.ParticleSystem("particles", 2000, scene)
		// Texture of each particle
		particleSystem.particleTexture = new BABYLON.Texture("./assets/textures/particles/rain.png", scene)

		// Where the particles come from
		particleSystem.emitter = this.fountain // the starting object, the emitter
		particleSystem.minEmitBox = new BABYLON.Vector3(0, 0, -35) // Starting all from
		particleSystem.maxEmitBox = new BABYLON.Vector3(0, -20, 15) // To...

		// Colors of all particles
		particleSystem.color1 = new BABYLON.Color4(1.0, .5, 1.0, 1.0)
		particleSystem.color2 = new BABYLON.Color4(.8, 0, .41, 1.0)
		particleSystem.colorDead = new BABYLON.Color4(1.0, 1.0, 1.0, 1.0)

		// Size of each particle (random between...
		particleSystem.minSize = 1
		particleSystem.maxSize = 1.75

		// Life time of each particle (random between...
		particleSystem.minLifeTime = 3
		particleSystem.maxLifeTime = 4

		// Emission rate
		particleSystem.emitRate = 500

		// Blend mode : BLENDMODE_ONEONE, or BLENDMODE_STANDARD
		particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD

		// Set the gravity of all particles
		particleSystem.gravity = new BABYLON.Vector3(0, 0, 0)

		// Direction of each particle after it has been emitted
		particleSystem.direction1 = new BABYLON.Vector3(0, -8, 8)
		particleSystem.direction2 = particleSystem.direction1

		// Angular speed, in radians
		particleSystem.minAngularSpeed = 0
		particleSystem.maxAngularSpeed = 0

		// Speed
		particleSystem.minEmitPower = 2
		particleSystem.maxEmitPower = 4
		particleSystem.updateSpeed = 2

		// Start the particle system
		particleSystem.start()
		particleSystem.maxEmitBox.y = 0
		particleSystem.updateSpeed = .01
	}

	/**
	 * Disposes of the particles
	 */
	dispose () {
		// Dispose
	}

	/**
	 * Updates the particle system
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		// Update
	}
}
