/**
 * Powerup Renderer
 * @module PowerupRenderer
 * @description Renderer for powerups
 */
export default class PowerupRenderer {
	/**
	 * @constructor
	 * @param {String} type Type of powerup
	 * @param {BABYLON.Vector3} [position] Position to render at
	 */
	constructor (type, position = new BABYLON.Vector3(0, 0, 0)) {
		this.delta = 0
		this.position = this.core_position = position
		this.type = type
	}

	update (dt) {
		const { x, y, z } = this.core_position

		this.delta += dt
		this.position.y = this.core_position.y + 1 * Math.sin(this.delta)
	}
}
