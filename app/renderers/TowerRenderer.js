// Classes
import BlankMesh from "../meshes/BlankMesh.js"

// Constants
export const MISSING_SEGMENT = 10
export const SEGMENT_COUNT = 21
export const SEGMENT_SIZE = 10

/**
 * Tower Renderer
 * @module TowerRenderer
 */
export default class TowerRenderer {
	/**
	 * @constructor
	 * @param {BABYLON.Scene} scene Scene to add renderer to
	 * @param {Number} [segment_size] Size of each segment
	 */
	constructor (scene, segment_size = SEGMENT_SIZE) {
		// Internalise the scene
		this.scene = scene

		this.point = new BlankMesh()

		// Setup offset
		this.offset = new BABYLON.Vector3(0, 0, 0)

		// Create billboard
		this.createBillboard()

		// Create tower segments
		this.createTowerSegments(segment_size)
	}

	/**
	 * Creates the major logo billboard
	 */
	createBillboard () {
		const WIDTH = SEGMENT_SIZE * .9
		const HEIGHT = WIDTH / 8

		// Create billboard
		this.billboard = BABYLON.MeshBuilder.CreatePlane("BILLBOARD", {
			height: HEIGHT,
			width: WIDTH,
		}, this.scene)
		this.billboard.setParent(this.point)

		// Position billboard correctly
		this.billboard.position.y = HEIGHT / 2
		this.billboard.position.z = 4.75
		this.billboard.rotation.y = Math.PI

		// Give billboard correct material
		const DIFFUSE_PATH = "./assets/textures/tower/billboard.png"
		const material = new BABYLON.StandardMaterial("BILLBOARD_MATERIAL", this.scene)
		material.diffuseTexture = new BABYLON.Texture(DIFFUSE_PATH, this.scene)
		this.billboard.material = material
	}

	/**
	 * Creates all the segments of the tower
	 * @param {Number} segment_size Size of each segment
	 */
	createTowerSegments (segment_size) {
		// All segments in an array
		this.segments = []

		// Create all segments
		for (let num = 0; num < SEGMENT_COUNT; num++) {
			// Do not create the missing segment
			if (num <= MISSING_SEGMENT) continue

			// Create segment
			const ID = `tower_mesh_${num}`
			let segment
			if (num === 0) {
				segment = BABYLON.MeshBuilder.CreateBox(ID, {
					size: segment_size,
				}, this.scene)
			} else {
				const p1 = BABYLON.MeshBuilder.CreatePlane("p1", {
					size: segment_size,
				})
				const p2 = BABYLON.MeshBuilder.CreatePlane("p2", {
					size: segment_size,
				})
				p1.position.x = segment_size / 2
				p1.rotation.y = -Math.PI / 2
				p2.position.z = segment_size / 2
				p2.rotation.y = -Math.PI
				segment = BABYLON.Mesh.MergeMeshes([p1, p2], true)
			}
			segment.setParent(this.point)

			// Position the segment
			segment.position.y = -(segment_size / 2 + segment_size * num)

			// Add segment to array of segments
			this.segments.push(segment)
		}
	}

	get position () {
		return this.point.position.y
	}
	set position (y) {
		this.point.position.y = y
	}
}
