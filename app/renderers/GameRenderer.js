// Classes
import BlankMesh from "../meshes/BlankMesh.js"
import BubbleParticles from "../particles/BubbleParticles"
import CarMesh from "../meshes/CarMesh.js"
import MultiMesh from "../meshes/MultiMesh.js"

// Constants
import { MODEL_PATH } from "../constants/paths.js"
import { STATE_ARENA_DEFAULT } from "../constants/tower.js"
import { UI_HEIGHT } from "../constants/ui.js"

/**
 * Game Renderer
 * @module GameRenderer
 */
export default class GameRenderer {
	/**
	 * @constructor
	 * @param {Object} opts Options for starting game
	 * @param {Object} [data] Extra data for starting game
	 * @param {Function} [on_load] Function to call on load
	 */
	constructor (opts, data = {}, on_load = () => {}) {
		this.map = opts.map
		this.opts = opts
		this.start_data = data

		// Save callbacks
		this.onLoad = on_load

		// Create BABYLON components
		this.origin = new BlankMesh()
		const scale = 2 / this.map.height
		this.origin.scaling.set(scale, scale, scale)

		this.scene = Tower.scene

		window.gr = this

		// Create UI for showing player data
		this.ui = window.yyy = this.createUI()
		this.ui.vScale = 0

		// Load materials
		this.mats = {
			block: this.createMaterial("block", this.scene),
			lava: this.createMaterial("lava", this.scene),
			tiles: {
				blue: this.createMaterial("tile_blue", this.scene),
				red: this.createMaterial("tile_red", this.scene),
				white: this.createMaterial("tile_white", this.scene, true),
			},
		}

		this.ultimates = []

		// Will contain all meshes
		this.meshes = {}

		// Create player renderers
		this.players = opts.players.map((player, key) => {
			const mesh = new CarMesh(player.name, this.scene, {
				body: player.body,
				wheel: player.wheel,
			})
			mesh.tag = `player_${key}`
			mesh.createComboBox(this.ui)
			mesh.createTag(this.ui)
			mesh.createUI(this.ui, key)
			mesh.setParent(this.origin)
			mesh.setupParticles()
			return mesh
		})

		// Create blocks
		this.blocks = this.start_data.blocks.reduce((objects, object) => {
			const x = object.position.x
			const y = object.position.z

			return [...objects, { owner: object.alignment, x, y }]
		}, [])

		this.wave_phase = 0

		this.setupMutation()

		// Load assets
		this.loadAssets(this.scene, data)

		this.pulse_material = new BABYLON.StandardMaterial("pulse", this.scene)
		const y = new BABYLON.Texture("assets/textures/pulse_ring.png")
		this.pulse_material.diffuseTexture = y.clone()
		this.pulse_material.diffuseTexture.hasAlpha = true
		this.pulse_material.emissiveTexture = y.clone()
		this.pulse_material.emissiveTexture.hasAlpha = true
		this.pulse_material.disableLighting = true
		this.pulse_material.useAlphaFromDiffuseTexture = true
	}

	/**
	 * Adds a mesh task to the loader
	 * @param {BABYLON.AssetsManager} loader Built in loader
	 * @param {String} id ID to save task as
	 * @returns {*} New mesh task
	 */
	addMeshTask (loader, id) {
		const task = loader.addMeshTask(id, "", MODEL_PATH, `josef_${id}.obj`)
		task.onSuccess = (data) => {
			data.loadedMeshes[0].visibility = false
		}
		return task
	}

	/**
	 * Creates a material
	 * @param {String} id ID for new material
	 * @param {BABYLON.Scene} scene Scene to add material to
	 * @param {Boolean} transparent Transparent texture or not
	 * @returns {BABYLON.StandardMaterial} Newly created material
	 */
	createMaterial (id, scene, transparent) {
		const path = `./assets/textures/demo_${id}.png`
		const mat = new BABYLON.StandardMaterial(`mat_${id}`, scene)
		mat.diffuseTexture = new BABYLON.Texture(path, scene)
		if (transparent) {
			mat.diffuseTexture.hasAlpha = true
		}
		return mat
	}

	/**
	 * Create a new UI
	 * @returns {BABYLON.GUI.AdvancedDynamicTexture} Newly created UI
	 */
	createUI () {
		// Setup the UI
		const UI = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI")
		UI.idealHeight = UI_HEIGHT
		return UI
	}

	/**
	 * Dispose of this game renderer
	 */
	dispose () {
		// Set that self is not loaded
		this.loaded = false

		// Dispose meshes
		this.blocks.forEach((block) => {
			block.tile.dispose()
		})
		this.players.forEach((player) => player.dispose())
		this.powerups.forEach((powerup) => powerup.dispose())

		this.ui.dispose()
		this.particles.forEach((particles) => {
			particles.dispose()
		})
	}

	/**
 	 * Determines the body part with the given ID
	 * @param {String} id ID of body to find
	 * @returns {*} Found body
	 */
	getBodyWithID (id) {
		return window.PARTS.bodies.find((body) => body.id === id)
	}

	/**
	 * Interprets the lava mesh
	 * @param {Object} meshdata Data of mesh to interpret
	 */
	interpretLavaMesh (meshdata) {
		// Determine mesh
		const source_mesh = meshdata.loadedMeshes[0]

		// Array to contain lava
		this.lava = this.start_data.hazards.reduce((objects, object, key) => {
			// Determine coordinates
			const x = object.position.x
			const y = object.position.z

			// Clone and place mesh
			const mesh = source_mesh.clone()
			mesh.setParent(this.origin)
			mesh.position.x = x
			mesh.position.z = y
			mesh.material = this.mats.lava

			return [...objects, { mesh, x, y }]
		}, [])

		// Destroy the original mesh
		source_mesh.dispose()
	}

	/**
	 * Returns whether or not the renderer has loaded all required assets
	 * @returns {Boolean} Whether the renderer has loaded all assets
	 */
	isLoaded () {
		return this.loaded
	}

	/**
	 * Creates assets manager and loads assets
	 * @param {BABYLON.Scene} scene Scene to load assets for
	 * @returns {BABYLON.AssetsManager} Loader object
	 */
	loadAssets (scene) {
		// Loader for assets
		const loader = new BABYLON.AssetsManager(scene)
		loader.useDefaultLoadingScreen = false

		// Add tasks to the loader
		const tasks = {
			lava: this.addMeshTask(loader, "lava_test"),
			tile: this.addMeshTask(loader, "tile_test"),
		}

		// Load powerups
		tasks.powerups = {
			boost: loader.addMeshTask("POWERUP_BOOST", "", MODEL_PATH + "powerup_boost/", "powerup_boost.gltf"),
		}
		this.powerups = []

		tasks.powerups.boost.onSuccess = (data) => {
			// debugger
			// const material = data.loadedMeshes[1].material.clone()
			// this.meshes.POWERUP_BOOST = BABYLON.Mesh.MergeMeshes(data.loadedMeshes, true, true)
			// this.meshes.POWERUP_BOOST.material = material
			// this.meshes.POWERUP_BOOST.visibility = false
			this.meshes.POWERUP_BOOST = new MultiMesh("boost", this.scene, [data.loadedMeshes[0]])
			this.meshes.POWERUP_BOOST.position.y = -10
			// this.meshes.POWERUP_BOOST.show = true
		}

		// Register mesh success callbacks
		tasks.lava.onSuccess = this.interpretLavaMesh.bind(this)
		tasks.tile.onSuccess = (meshdata) => {
			this.meshes.tile = meshdata.loadedMeshes[0]
		}

		// Callback for after loader has been completed
		loader.onFinish = () => {
			// Indicate that the loader has completed
			this.loaded = true

			// Create tiles for meshes
			this.placeTilesOnBlocks(this.blocks)

			this.scene.meshes.forEach((m) => {
				if (m.name === "_mm1") m.dispose()
			})

			Tower.setState(STATE_ARENA_DEFAULT)
			this.origin.position.x = -5
			this.origin.position.y = 0
			this.origin.position.z = -5
		}

		// Begin loading of assets
		loader.load()

		return loader
	}

	/**
	 * Places tiles on blocks
	 * @param {Array} blocks All blocks on which to place tiles
	 */
	placeTilesOnBlocks (blocks) {
		// Place tiles on all blocks
		blocks.forEach((block) => {
			// Create tile
			const tile = this.meshes.tile.clone()
			tile.setParent(this.origin)

			// Position and skin tile
			tile.position.x = block.x
			tile.position.z = block.y
			tile.material = this.mats.tiles.white

			// Assign tile to block
			block.tile = tile
		})

		// Dispose of tile mesh
		this.meshes.tile.dispose()
	}

	setupMutation () {
		const towers = this.map.layers.find((layer) => {
			return layer.name === "towers"
		}).objects

		this.particles = this.particles || []

		towers.forEach((tower) => {
			const name = `mut_tow_${tower.name}`
			const cube = BABYLON.MeshBuilder.CreateBox(name, {
				width: 5,
				depth: 5,
				height: 0.1
			}, this.scene)
			cube.setParent(this.origin)
			cube.position.x = (tower.y + 10) / 4
			cube.position.y = -.1
			cube.position.z = (tower.x + 10) / 4
			cube.visibility = false
			const particles = new BubbleParticles(name + "_p", this.scene, cube)
			particles.start()
			this.particles.push(particles)
		})
	}

	/**
	 * Update the renderer with new data
	 * @param {Number} dt Delta time
	 * @param {Object} data Data to use to update renderer
	 */
	update (dt, data) {
		// Do not render when not loaded
		if (!this.isLoaded()) return
		this.ui.vScale = 1

		// Update entities
		this.updateBlocks(data.blocks)
		this.updatePlayers(data.players, dt, data.scores)
		this.updatePowerups(dt, data.powerups)
		this.updateUltimates(dt, data.ultimates)

		this.wave_phase = (this.wave_phase + dt / 2) % 1
		Tower.updateCameraZoom(5.5)

	}

	/**
	 * Updates all blocks with given data
	 * @param {Object[]} block_updates Updates for blocks
	 */
	updateBlocks (block_updates) {
		this.blocks.forEach((block, key) => {
			const alignment = block_updates[key].alignment
			const phase = block_updates[key].phase
			if (alignment) {
				let tex
				if (alignment === "BLUE") {
					tex = "blue"
				} else if (alignment === "RED") {
					tex = "red"
				}
				block.tile.material = this.mats.tiles[tex]
			}
			const wave = this.wave_phase + (block.tile.position.x % 40) / 40 + block_updates[key].timer
			const newY = phase - 1 + (Math.sin(wave * Math.PI * 2) / 6) + .3
			const diff = (newY - block.tile.position.y) * .5
			if (Math.abs(diff) < .3) {
				block.tile.position.y = newY
			} else {
				block.tile.position.y += .3 * Math.sign(newY - block.tile.position.y)
			}
		})
	}

	/**
	 * Update players with given data
	 * @param {Object[]} player_updates Updates for players
	 * @param {Number} dt Delta time
	 * @param {Array} scores scores
	 */
	updatePlayers (player_updates, dt, scores) {
		this.players.forEach((player, key) => {
			const data = player_updates[key]
			if (data.events.smashed) { Tower.addShake(.2) }
			if (data.events.drop) { Tower.addShake(.05) }
			if (data.events.lasering) { Tower.addShake(dt * .75) }
			if (data.events.died) {
				Sound.playSFX("death")
			}
			player.update(data, dt)
			player.updateScores(scores[key])
			player.updateUI(data)
		})
	}

	/**
	 * Update powerups with given data
	 * @param {Number} dt Delta time
	 * @param {Object[]} powerup_updates Updates for powerups
	 */
	updatePowerups (dt, powerup_updates) {
		const renderer_ids = this.powerups.map((powerup) => powerup.id)
		const controller_ids = []

		powerup_updates.forEach((data, key) => {
			// Add own data to array
			controller_ids.push(data.id)

			// Add new powerups
			const id = renderer_ids.indexOf(data.id)
			if (id === -1) {
				const mesh = this.meshes[data.type].clone()
				mesh.visibility = true
				mesh.setParent(this.origin)
				mesh.id = data.id
				mesh.position = data.position
				mesh.type = data.type

				this.powerups.push(mesh)
			}
		})

		// Rotate powerups
		this.powerups.forEach((powerup) => {
			powerup.rotation.y += dt * 2
		})

		// Remove dead powerups
		renderer_ids.forEach((id) => {
			if (controller_ids.indexOf(id) === -1) {
				const index = this.powerups.findIndex((powerup) => {
					return powerup.id === id
				})
				this.powerups[index].dispose()
				this.powerups.splice(index, 1)
				Sound.playSFX("car_pickup")
			}
		})
	}

	/**
	 * ults
	 * @param {Number} dt Delta time
	 * @param {Object[]} ults ults
	 */
	updateUltimates (dt, ults) {
		const ult_list = []

		ults.forEach((ult) => {
			const found_ult = this.ultimates.find((ultimate) => {
				return ultimate.id === ult.id
			})

			if (found_ult) {
				if (found_ult.name === "PULSE_ULTIMATE") {
					const scale = ult.phase * 2
					found_ult.scaling.set(scale, scale, scale)
					found_ult.material.alpha = Math.min((1 - ult.phase) * 2, 1)
				}
			} else {
				if (ult.name === "PULSE_ULTIMATE") {
					const mesh = BABYLON.Mesh.CreateGround(ult.id, ult.size, ult.size, 2, this.scene)
					mesh.setParent(this.origin)
					mesh.material = this.pulse_material.clone()
					mesh.name = ult.name
					mesh.position.x = ult.x
					mesh.position.y = 10
					mesh.position.z = ult.z
					mesh.scaling.set(ult.phase, ult.phase, ult.phase)
					this.ultimates.push(mesh)
				} else if (ult.name === "ANTI_ENERGY_ULTIMATE") {
					this.players.forEach((player) => {
						if (player.tag === ult.owner) {
							player.aeParticles._particles.disposeOnStop = false
							player.aeParticles.start()
						}
					})
					this.ultimates.push({
						dumb: true,
						id: ult.id,
						name: ult.name,
						owner: ult.owner,
					})
				}
			}

			ult_list.push(ult.id)
		})

		this.ultimates = this.ultimates.filter((ult) => {
			if (ult_list.indexOf(ult.id) === -1) {
				this.players.forEach((player) => {
					if (player.tag === ult.owner) {
						player.aeParticles.stop()
					}
				})

				// Kill ultimate
				if (!ult.dumb) {
					this.scene.removeMesh(ult)
				}
				return false
			}
			return true
		})
	}
}
