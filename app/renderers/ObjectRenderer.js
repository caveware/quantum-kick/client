// Constants
import { MODEL_PATH } from "../constants/paths.js"

/**
 * Object Renderer
 * @module ObjectRenderer
 */
export default class ObjectRenderer {
	/**
	 * @constructor
	 * @param {String} id Unique identifier
	 * @param {BABYLON.scene} scene Scene to add object to
	 * @param {Object} details Extra details
	 */
	constructor (id, scene, details) {
		// Internal variables
		this.details = details
		this.id = id
		this.scene = scene
	}

	/**
	 * Performed to destroy renderer
	 */
	dispose () {
		this.material.dispose()
		this.mesh.dispose()
	}

	/**
	 * Get x position
	 * @returns {Number} x position
	 */
	get x () { return this.mesh.position.x }

	/**
	 * Get y position
	 * @returns {Number} y position
	 */
	get y () { return this.mesh.position.y }

	/**
	 * Get z position
	 * @returns {Number} z position
	 */
	get z () { return this.mesh.position.z }

	/**
	 * Set x position
	 * @param {Number} x x position
	 */
	set x (x) { this.mesh.position.x = x }

	/**
	 * Set y position
	 * @param {Number} y y position
	 */
	set y (y) { this.mesh.position.y = y }

	/**
	 * Set z position
	 * @param {Number} z z position
	 */
	set z (z) { this.mesh.position.z = z }

	/**
	 * Load material
	 * @param {Object} textures Textures to use
	 * @returns {*} Newly created material
	 */
	loadMaterial (textures) {
		const id = `mat_${this.id}`
		const scene = this.scene
		let mat

		if (textures.diffuse) {
			// Define texture paths
			const diffuse = textures.diffuse
			const emissive = textures.emissive
			const specular = textures.specular

			// Create standard material
			mat = new BABYLON.StandardMaterial(id, scene)
			mat.diffuseTexture = new BABYLON.Texture(diffuse, scene)

			// Apply emissive and specular textures
			if (emissive) {
				mat.emissiveTexture = new BABYLON.Texture(emissive, scene)
			}
			if (specular) {
				mat.specularTexture = new BABYLON.Texture(specular, scene)
			}
		} else {
			// Define texture paths
			const base = textures.base
			const mrt = textures.mrt

			// Create PBR material
			mat = new BABYLON.PBRMetallicRoughnessMaterial(id, scene)
			mat.baseColor = new BABYLON.Color3(1, 1, 1)
			mat.metallic = 1
			mat.roughness = 1
			mat.baseTexture = new BABYLON.Texture(base, scene)

			// Apply metallic roughness texture
			if (textures.mrt) {
				mat.metallicRoughnessTexture = new BABYLON.Texture(mrt, scene)
			}
		}

		// Apply material
		this.material = this.mesh.material = mat

		return mat
	}

	/**
	 * Creates task to load the mesh and material
	 * @param {Object} loader Loader to load this mesh with
	 * @param {Object} [parent] Parent of the mesh
	 * @returns {Object} Task
	 */
	loadMesh (loader, parent) {
		// Load mesh task
		const id = `mesh_${this.id}`
		const task = loader.addMeshTask(id, "", MODEL_PATH, this.details.model.slice(14))

		// Create loader task
		task.onSuccess = (meshdata) => {
			const object = {
				height: 32,
				width: 32,
				x: 0,
				y: 0,
			}

			// Determine mesh
			this.mesh = meshdata.loadedMeshes[0]
			if (parent) this.mesh.setParent(parent)

			// Determine coordinates
			const x = (object.y + object.height / 2) / 4
			const y = (object.x + object.width / 2) / 4

			// Clone and place mesh
			this.mesh.position.x = x
			this.mesh.position.z = y

			// Load material
			this.loadMaterial(this.details.textures)
		}

		return task
	}
}
