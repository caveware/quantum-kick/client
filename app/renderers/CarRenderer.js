// Classes
import ObjectRenderer from "./ObjectRenderer.js"

/**
 * Car Renderer
 * @module CarRenderer
 */
export default class CarRenderer extends ObjectRenderer {
	/**
	 * Updates the car renderer
	 * @param {Object} data Contains update data
	 */
	update (data) {
		this.x = data.position.x
		this.y = data.position.y
		this.z = data.position.z

		const r = data.rotation
		this.mesh.rotation.set(r.x, r.y, r.z)
	}
}
