// Libraries
import electron from "electron"

// Managers
import InputManager from "./managers/InputManager.js"
import LanguageManager from "./managers/LanguageManager.js"
import SettingsManager from "./managers/SettingsManager.js"
import SoundManager from "./managers/SoundManager.js"
import StateManager from "./managers/StateManager.js"
import TowerManager from "./managers/TowerManager.js"
import UIManager from "./managers/UIManager.js"

// Constants
import Maps from "../common/maps/index.js"

// Globals
window.Maps = Maps
window.TWEEN = require("@tweenjs/tween.js")

// Platform specific hacks
if (window.Windows) {
	// XBOX hacks
	navigator.gamepadInputEmulation = "gamepad"
	window.IS_XBOX = true
	window.Windows.UI.ViewManagement.ApplicationView
		.getForCurrentView()
		.setDesiredBoundsMode(window.Windows.UI.ViewManagement.ApplicationViewBoundsMode.useCoreWindow)
} else {
	window.IS_XBOX = false
	window.remote = electron.remote
}

// Load constants
require("./constants.js")

// Create BABYLON engine
window.Engine = new BABYLON.Engine(document.getElementById("game"), true)
window.addEventListener("resize", function () {
	window.Engine.resize()
})
window.Engine.enableOfflineSupport = false

// Create managers
window.Input = new InputManager()
window.Language = new LanguageManager()
window.Sound = new SoundManager()
window.StateManager = new StateManager()
window.Tower = new TowerManager()
window.UI = new UIManager()

Sound.playBGM("menu")

window.Settings = new SettingsManager()
