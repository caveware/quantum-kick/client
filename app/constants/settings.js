/**
 * Whether or not the game is in fullscreen
 * @constant
 */
export const FULLSCREEN = "FULLSCREEN"

/**
 * Whether ot not the game has been configured
 * @constant
 */
export const IS_CONFIGURED = "IS_CONFIGURED"
