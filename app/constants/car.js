// Internal constructors
const _buildBodyItem = (id, name, mesh, wheels) => ({
	id: `BODY_${id}`,
	mesh,
	name,
	wheels,
})
const _buildWheelItem = (id, name, mesh, config) => ({
	config,
	id: `WHEEL_${id}`,
	mesh,
	name,
})

// Defaults
export const DEFAULT_BODY = "BODY_BASE_RED"
export const DEFAULT_WHEEL = "WHEEL_HEXAGON"

// All body meshes for cars
export const BODY_MESHES = [
	// _buildBodyItem("BASE", "Default", "saucer_base/saucer_base.gltf", [
	// 	{ x: 1.01, z: 1.2 },
	// 	{ x: 1.18, z: -1.15 },
	// ]),
	// _buildBodyItem("HEX", "Hexagon", "saucer_hex/saucer_hex.gltf", [
	// 	{ x: 1, z: 1 },
	// 	{ x: 1, z: -1 },
	// ]),
	_buildBodyItem("BASE_RED", "BR", "saucer_base/saucer_base_red.gltf", [
		{ x: 1.01, z: 1.2 },
		{ x: 1.18, z: -1.15 },
	]),
	_buildBodyItem("BASE_BLUE", "BB", "saucer_base/saucer_base_blue.gltf", [
		{ x: 1.01, z: 1.2 },
		{ x: 1.18, z: -1.15 },
	]),
	_buildBodyItem("WOAH_RED", "WR", "saucer_woah/saucer_woah_red.gltf", [
		{ x: 1.01, z: 1.2 },
		{ x: 1.18, z: -1.15 },
	]),
	_buildBodyItem("WOAH_BLUE", "WB", "saucer_woah/saucer_woah_blue.gltf", [
		{ x: 1.01, z: 1.2 },
		{ x: 1.18, z: -1.15 },
	]),
]

// All wheel meshes for cars
export const WHEEL_MESHES = [
	_buildWheelItem("HEXAGON", "Hexagon", "car_wheel_hexagon/car_wheel_hexagon.obj", {
		acceleration: 1,
	}),
]
