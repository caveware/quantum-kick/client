// Libraries
import { easing } from "popmotion"

/**
 * ID for the camera
 */
export const CAMERA_ID = "TOWER_CAMERA_ID"

/**
 * Default duration of tween
 * @constant
 */
export const DEFAULT_TWEEN_DURATION = 250

/**
 * Default easing method
 * @constant
 */
export const DEFAULT_TWEEN_EASE = easing.easeInOut

/**
 * Floor for the local lobby menu
 * @constant
 */
export const FLOOR_LOCAL = {
	id: "TOWER_FLOOR_LOCAL",
	model: "floor_local.gltf",
	path: "floor_local/",
}

/**
 * Floor for the local lobby menu
 * @constant
 */
export const FLOOR_MAP_CITADEL = {
	id: "TOWER_FLOOR_MAP_CITADEL",
	model: "map_citadel_j.gltf",
	path: "map_citadel_j/",
}

/**
 * Floor for the options menu
 * @constant
 */
export const FLOOR_OPTIONS = {
	id: "TOWER_FLOOR_OPTIONS",
	model: "floor_options.gltf",
	path: "floor_options/",
}

/**
 * State for showing the arena clearly
 * @constant
 */
export const STATE_ARENA_DEFAULT = {
	id: "TOWER_STATE_ARENA_DEFAULT",
	position: { x: 15, y: 12.5, z: 15 },
	target: { x: 5, y: 4.5, z: 5 },
	zoom: 4,
}

/**
 * State for showing the logo closeup
 * @constant
 */
export const STATE_LOGO_CLOSEUP = {
	id: "TOWER_STATE_LOGO_CLOSEUP",
	position: { x: 0, y: .5, z: 10 },
	target: { x: 0, y: .5, z: 0 },
	zoom: 3,
}

/**
 * Staring at nothing.
 * @constant
 */
export const STATE_NOTHING = {
	id: "TOWER_STATE_ARENA_NOTHING",
	position: { x: 25, y: 22.5, z: 25 },
	target: { x: 25, y: 24.5, z: 25 },
	zoom: 4,
}

/**
 * State for showing the rooftop clearly
 * @constant
 */
export const STATE_SCENIC_ROOFTOP = {
	id: "TOWER_STATE_SCENIC_ROOFTOP",
	position: { x: 30, y: 12.5, z: 20 },
	target: { x: 20, y: 7.5, z: 10 },
	zoom: 10,
}
