/**
 * UI constants
 */
export const UI_HEIGHT = 1080

// Events
export const EVENT_CHANGE_LANGUAGE = "UIEVT_CHANGE_LANGUAGE"
export const EVENT_SELECT_MAIN_MENU_ITEM = "UIEVT_SELECT_MAIN_MENU_ITEM"
export const EVENT_SELECT_OPTIONS_MENU_ITEM = "UIEVT_SELECT_OPTIONS_MENU_ITEM"

// Gameplay events
export const EVENT_GP_UPDATE_PLAYERS = "GPEVT_UPDATE_PLAYERS"
export const EVENT_GP_UPDATE_TIMER = "GPEVT_UPDATE_TIMER"