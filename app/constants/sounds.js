/**
 * All sounds to be loaded
 */
export const BGM = {
	game_1: "bgm_game_1.ogg",
	menu: "bgm_menu.mp3",
	result: "bgm_result.ogg",
}
export const SFX = {
	announcer_1: "announcer/1.ogg",
	announcer_2: "announcer/2.ogg",
	announcer_3: "announcer/3.ogg",
	announcer_go: "announcer/go.ogg",
	car_boost: "car/boost.ogg",
	car_crash: "car/crash.ogg",
	car_pickup: "car/pickup.ogg",
	car_rev: "car/rev.ogg",
	change_option: "change_option.ogg",
	death: "death.ogg",
	game_over: "game_over.ogg",
	hitting: "hitting.ogg",
	menu_item_back: "menu/item_back.ogg",
	menu_item_click: "menu/item_click.ogg",
	menu_item_hover: "menu/item_hover.ogg",
	press_start: "press_start.ogg",
	select_option: "select_option.ogg",
}
