/**
 * All routes
 */
export const BLANK = "/"
export const CAR_CUSTOMISATION = "/car-customisation"
export const ERROR = "/error"
export const FIRST_SETUP = "/first-setup"
export const GAMEPLAY = "/gameplay"
export const JOIN_LOBBY = "/join-lobby"
export const LOBBY_LIST = "/lobby-list"
export const LOCAL_LOBBY = "/local"
export const MAIN_MENU = "/main-menu"
export const OPTIONS_MENU = "/options-menu"
export const PRESS_START = "/press-start"
export const RESULTS_LOCAL = "/results-local"
export const RTX_CONTROLS = "/rtx-controls"
export const RTX_LOBBY = "/rtx-lobby"
export const RTX_SPLASH = "/rtx-splash"
export const TEAM_SELECTION = "/team-selection"
