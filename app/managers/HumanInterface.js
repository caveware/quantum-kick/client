/**
 * Interface between human input and code.
 * @module HumanInterface
 */
export default class HumanInterface {
	/**
	 * @constructor
	 * @param {InputManager} input_manager Input manager this is bound to
	 * @param {String} type Type of interface
	 * @param {Number} [number] Which controller number this interface is
	 */
	constructor (input_manager, type, number = 0) {
		this.input_manager = input_manager
		this.number = number
		this.type = type
	}

	/**
	 * Disposes the interface
	 */
	dispose () {
		// Should do -something-
	}

	/**
	 * Check value of given input
	 * @param {Object[]} control_array Array of controls
	 * @param {Number} deadzone Deadzone of control
	 * @returns {Boolean} Value of given control
	 */
	get (control_array, deadzone) {
		const control = this.getActions(control_array)
		return this.input_manager.get(this.getID(), control, undefined, deadzone)
	}

	/**
	 * Get actions for given control
	 * @param {Array} control Control to determine actions for
	 * @returns {Array} Array of control actions
	 */
	getActions (control) {
		let controls = []

		const input_type = this.type.toUpperCase()

		control.forEach((ctrl) => {
			ctrl = (ctrl.constructor === Array) ? ctrl : [ ctrl ]
			if (ctrl[0].startsWith(input_type)) {
				ctrl.forEach((c) => {
					const val = window[input_type][c.slice(4)]
					if (input_type === "PAD") {
						controls.push(val)
					} else {
						controls.push(val[0])
					}
				})
			}
		})

		return controls
	}

	/**
	 * Returns the control ID of this interface
	 * @returns {Number|String} Control ID
	 */
	getID () {
		return this.type === "key" ? "key" : this.number
	}

	/**
	 * Check if given input was pressed
	 * @param {Object[]} control_array Array of controls
	 * @param {Number} mod Modification to input
	 * @returns {Boolean} Whether or not the input was pressed
	 */
	pressed (control_array, mod = 1) {
		const control = this.getActions(control_array)
		return this.input_manager.pressed(this.getID(), control, mod)
	}

	/**
	 * Check if given input was released
	 * @param {Object[]} control_array Array of controls
	 * @param {Number} mod Modification to input
	 * @returns {Boolean} Whether or not the input was released
	 */
	released (control_array, mod = 1) {
		const control = this.getActions(control_array)
		return this.input_manager.released(this.getID(), control, mod)
	}
}
