// Libraries
import { tween } from "popmotion"

// Classes
import TowerRenderer from "../renderers/TowerRenderer.js"

// Constants
import { MODEL_PATH } from "../constants/paths.js"
import * as TOWER from "../constants/tower.js"

/**
 * Tower Manager
 * @module TowerManager
 * @description Controls the main tower for the whole game
 */
export default class TowerManager {
	/**
	 * @constructor
	 * @param {String} [state] State to start on
	 */
	constructor (state = TOWER.STATE_NOTHING) {
		// Create scene with a black background colour
		this.scene = new BABYLON.Scene(window.Engine)
		this.scene.clearColor = new BABYLON.Color4(0, 0, 0, 0.0000000000000001)

		// Setup the orthographic camera
		this.setupOrthographicCamera()

		// Setup the lighting
		this.setupLighting()

		// Setup the tower
		this.setupTower()

		// Setup the current state
		this.setState(state)
	}

	addShake (value) {
		this.camera_shake = Math.min(1, value + this.camera_shake)
	}

	/**
	 * Clears the current floor.
	 */
	clearFloor () {
		if (this.mesh) {
			// Remove mesh
			this.scene.removeMesh(this.mesh)

			// Destroy mesh
			this.mesh.dispose()
			this.mesh = null
		}
	}

	/**
	 * Moves to a given floor
	 * @param {String} floor Floor to move to
	 * @param {String} [direction] Direction to move in
	 * @param {Function} [callback] Function to call after tween
	 * @param {Number} [duration] Length of transition
	 */
	goToFloor (floor, direction = "down", callback = () => {}, duration = 300) {
		// Do not do anything if floor is cuirrent floor
		if (this.current_floor && floor.id === this.current_floor.id) return

		if (this.tower_tween) this.tower_tween.stop()
		let to = 110

		if (this.inside_tower) to += (direction === "down") ? 100 : -100

		this.next_floor = floor

		this.inside_tower = true
		if (!this.duration) {
			this.tower.position = 110
			Tower.updateTowerPosition(110)
			callback()
			return
		}

		this.tower_tween = tween({
			duration: duration,
			from: this.tower.position,
			to: to,
		}).start({
			complete: () => {
				this.tower.position = 110
				callback()
			},
			update: this.updateTowerPosition.bind(this),
		})
	}

	/**
	 * Transitions to the rooftop state.
	 */
	goToRooftop () {
		// Only go to rooftop if inside tower
		if (!this.inside_tower) return

		// Kill existing tween
		if (this.tower_tween) this.tower_tween.stop()

		// Tween up to the rooftop
		this.current_floor = null
		this.inside_tower = false
		this.tower_tween = tween({
			duration: 200,
			from: this.tower.position,
			to: 0,
		}).start({
			complete: () => {
				this.tower.position = 0
			},
			update: this.updateTowerPosition.bind(this),
		})
	}

	/**
	 * Loads the next floor.
	 */
	loadNextFloor () {
		const floor = this.next_floor
		if (floor) {
			if (this.mesh) this.clearFloor()

			// Create loader
			const loader = new BABYLON.AssetsManager(this.scene)
			loader.useDefaultLoadingScreen = false

			// Load mesh
			if (floor.id) {
				const mesh = loader.addMeshTask(floor.id, "", MODEL_PATH + floor.path, floor.model)
				mesh.onSuccess = (data) => {
					this.setMesh(data.loadedMeshes[0])
				}
				loader.load()
				this.current_floor = floor
			} else {
				const Floor = floor
				const inst = new Floor(this.scene)
				this.setMesh(inst)
				this.current_floor = inst
			}

			// Set floor details
			this.next_floor = null
		}
	}

	/**
	 * Render the scene itself
	 * @param {Number} dt Delta time
	 */
	render (dt) {
		// Update floor, if possible
		if (this.current_floor && this.current_floor.update) {
			this.current_floor.update(dt)
		}

		// Update the camera size and zoom
		this.updateOrthographicCamera(dt)

		// Render the scene
		this.scene.render()
	}

	/**
	 * Sets the mesh on screen
	 * @param {BABYLON.Mesh} mesh Mesh to show
	 */
	setMesh (mesh) {
		this.mesh = mesh
		this.mesh.setParent(this.tower.point)
		this.mesh.position.y = -110
	}

	/**
	 * Sets the current state
	 * @param {Object} state Name of state to set
	 * @param {Function} [callback] Callback for after tween completed
	 */
	setState (state, callback = () => {}) {
		if (!this.state) {
			// Immediately set camera variables
			this.updateCameraPosition(state.position)
			this.updateCameraTarget(state.target)
			this.updateCameraZoom(state.zoom)
		} else if (this.state.id !== state.id) {
			// Tween settings
			const TWEEN_DURATION = state.duration || TOWER.DEFAULT_TWEEN_DURATION
			const TWEEN_EASE = state.ease || TOWER.DEFAULT_TWEEN_EASE

			// Complete camera position tween
			if (this.camera_position_tween) {
				callback()
				this.camera_position_tween.seek(1)
				this.camera_position_tween.stop()
			}

			// Complete camera target tween
			if (this.camera_target_tween) {
				this.camera_target_tween.seek(1)
				this.camera_target_tween.stop()
			}

			// Complete camera zoom tween
			if (this.camera_zoom_tween) {
				this.camera_zoom_tween.seek(1)
				this.camera_zoom_tween.stop()
			}

			// Set camera position tween
			this.camera_position_tween = tween({
				duration: TWEEN_DURATION,
				ease: TWEEN_EASE,
				from: this.camera_position,
				to: state.position,
			}).start({
				complete: () => {
					callback()
					this.camera_position_tween = undefined
				},
				update: this.updateCameraPosition.bind(this),
			})

			// Set camera target tween
			this.camera_target_tween = tween({
				duration: TWEEN_DURATION,
				ease: TWEEN_EASE,
				from: this.camera_target,
				to: state.target,
			}).start({
				complete: () => { this.camera_target_tween = undefined },
				update: this.updateCameraTarget.bind(this),
			})

			// Set camera zoom tween
			this.camera_zoom_tween = tween({
				duration: TWEEN_DURATION,
				ease: TWEEN_EASE,
				from: this.camera_zoom,
				to: state.zoom,
			}).start({
				complete: () => { this.camera_zoom_tween = undefined },
				update: this.updateCameraZoom.bind(this),
			})
		}

		// Update state value
		this.state = state
	}

	/**
	 * Sets up the lighting for the scene
	 */
	setupLighting () {
		const ID = "global_light"
		const DIRECTION = new BABYLON.Vector3(-1, -2, -3)
		this.light = new BABYLON.DirectionalLight(ID, DIRECTION, this.scene)
	}

	/**
	 * Sets up the orthographic camera
	 */
	setupOrthographicCamera () {
		this.camera_shake = 0

		// Create the camera
		const ID = TOWER.CAMERA_ID
		const ZERO = BABYLON.Vector3.Zero()
		this.camera = new BABYLON.FreeCamera(ID, ZERO, this.scene)

		// Setup zoom and orthography
		this.camera.mode = BABYLON.Camera.ORTHOGRAPHIC_CAMERA
	}

	/**
	 * Sets up the tower renderer
	 */
	setupTower () {
		this.tower = new TowerRenderer(this.scene)
		this.inside_tower = false
	}

	/**
	 * Update where the camera is
	 * @param {Object} position Position of the camera
	 */
	updateCameraPosition (position) {
		this.camera_position = position
		this.camera.position.set(position.x, position.y, position.z)
	}

	/**
	 * Update where the camera faces
	 * @param {Object} target Where to face the camera
	 */
	updateCameraTarget (target) {
		this.camera_target = target
		this.camera.setTarget(new BABYLON.Vector3(target.x, target.y, target.z))
	}

	/**
	 * Update the camera zoom
	 * @param {Number} zoom Zoom of camera
	 */
	updateCameraZoom (zoom) {
		this.camera_zoom = zoom
	}

	/**
	 * Updates the camera size and zoom
	 */
	updateOrthographicCamera (dt) {
		this.camera_shake = Math.max(0, this.camera_shake - dt)

		const position = this.camera_position
		const shake = this.camera_shake * this.camera_shake / 20
		const x = position.x + (Math.random() - .5) * shake
		const y = position.y + (Math.random() - .5) * shake
		const z = position.z + (Math.random() - .5) * shake
		this.camera.position.set(x, y, z)

		// Determine camera ratio
		const {width, height} = Engine.getRenderingCanvas()
		const camera_ratio = width / height

		// Update the camera bounds
		this.camera.orthoBottom = -this.camera_zoom
		this.camera.orthoLeft = -this.camera_zoom * camera_ratio
		this.camera.orthoRight = this.camera_zoom * camera_ratio
		this.camera.orthoTop = this.camera_zoom
	}

	/**
	 * Sets the vertical tower position.
	 * @param {Number} value Position value
	 */
	updateTowerPosition (value) {
		if (this.inside_tower) {
			if (value < 60 && this.fell) {
				value += 100
				this.loadNextFloor()
			}
			if (value >= 60 && !this.fell) {
				this.fell = true
				this.loadNextFloor()
			}
			if (value > 160) {
				value -= 100
				this.loadNextFloor()
			}
		} else {
			this.fell = false
		}

		this.tower.position = value
	}
}
