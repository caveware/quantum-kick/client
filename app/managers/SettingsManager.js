// Constants
import { SUPPORTED_LANGUAGES } from "./LanguageManager.js"
import { DEFAULT_BODY, DEFAULT_WHEEL } from "../constants/car"

// Libraries
const Store = require("locallyjs").Store

/**
 * Settings Manager
 * @module SettingsManager
 */
export default class SettingsManager {
	/**
	 * @constructor
	 */
	constructor () {
		this.store = new Store()

		let locale = "en"
		if (!window.IS_XBOX) {
			locale = window.remote.app.getLocale().slice(0, 2)
			const win = window.remote.BrowserWindow.getAllWindows()[0]

			// Fullscreen
			if (this.get("FULLSCREEN") === null) {
				this.set("FULLSCREEN", win.isFullScreen())
			} else {
				win.setFullScreen(this.get("FULLSCREEN"))
			}
		}

		// Language
		if (!this.get("LANGUAGE")) {
			this.set("LANGUAGE", SUPPORTED_LANGUAGES.indexOf(locale) > -1 ? locale : "en")
		} else {
			Language.current = this.get("LANGUAGE")
		}

		// Name
		if (this.get("NAME") === null) {
			this.set("NAME", "Player")
		}

		// BGM
		if (this.get("BGM_VOLUME") !== null) {
			Sound.updateBGMVolume(this.get("BGM_VOLUME") / 100)
		} else {
			this.set("BGM_VOLUME", 100)
		}

		// SFX
		if (this.get("SFX_VOLUME") !== null) {
			Sound.updateSFXVolume(this.get("SFX_VOLUME") / 100)
		} else {
			this.set("SFX_VOLUME", 100)
		}

		// Lobby settings
		if (this.get("LOBBY_NAME") === null) {
			this.set("LOBBY_MAP", "default")
			this.set("LOBBY_MODE", "grid-control")
			this.set("LOBBY_NAME", "New Lobby")
			this.set("LOBBY_TEAMS", "GAME_1v1")
		}

		// Car configuration
		if (!this.get("CAR_CONFIGURATION")) {
			const DEFAULT = {
				body: DEFAULT_BODY,
				wheel: DEFAULT_WHEEL,
			}
			this.set("CAR_CONFIGURATION", [ DEFAULT, DEFAULT, DEFAULT, DEFAULT ])
		}
	}

	/**
	 * Retrieves value from store
	 * @param {String} key Key of stored value
	 * @returns {*} Value from store
	 */
	get (key) {
		return this.store.get(key)
	}

	/**
	 * Store a value in store with ID
	 * @param {String} key Key of value to store
	 * @param {*} value Value to store
	 */
	set (key, value) {
		switch (key) {
			case "BGM_VOLUME":
				Sound.updateBGMVolume(value / 100)
				break
			case "FULLSCREEN":
				const win = window.remote.BrowserWindow.getAllWindows()[0]
				win.setFullScreen(value)
				break
			case "LANGUAGE":
				Language.current = value
				break
			case "SFX_VOLUME":
				Sound.updateSFXVolume(value / 100)
				break
		}

		this.store.set(key, value)
	}
}
