// Classes
import HumanInterface from "./HumanInterface"

// Exports
export const CONTROL_TYPE_KEY = "key"
export const CONTROL_TYPE_PAD = "pad"
export const MAX_INTERFACES = 4

/**
 * Input Manager
 * @module InputManager
 */
export default class InputManager {
	/**
	 * @constructor
	 */
	constructor () {
		this.keys = {}
		this.last_keys = {}

		this.deadzone = .15
		this.gamepads = this.getGamepads()
		this.last_gamepads = this.getGamepads()

		this.all_inputs = [
			this.createHumanInterface(CONTROL_TYPE_KEY, 0),
			this.createHumanInterface(CONTROL_TYPE_PAD, 0),
			this.createHumanInterface(CONTROL_TYPE_PAD, 1),
			this.createHumanInterface(CONTROL_TYPE_PAD, 2),
			this.createHumanInterface(CONTROL_TYPE_PAD, 3),
		]

		this.setList([ this.all_inputs[0] ])
		this.setupListeners()

		this.show_player_count = true
	}

	/**
	 * Creates a new human interface
	 * @param {String} type Type of new interface
	 * @param {Number} [pad_number] Number of controller if pad
	 * @returns {HumanInterface} Newly created interface
	 */
	createHumanInterface (type = CONTROL_TYPE_PAD, pad_number = 0) {
		return new HumanInterface(this, type, pad_number)
	}

	/**
	 * Get the value of a control on a controller
	 * @param {Number} id ID of control
	 * @param {Array} control Control to get value of
	 * @param {Array} pads Pads to pick from
	 * @param {Number} deadzone Deadzone of controller inputs
	 * @returns {Number} Value of control
	 */
	get (id, control, pads, deadzone) {
		if (deadzone === undefined) deadzone = this.deadzone

		// If pads not given, determine list of pads
		if (!pads) {
			pads = (id === CONTROL_TYPE_KEY) ? this.keys : this.gamepad
		}

		// If checking for keyboard presses, just do so
		if (id === "key") {
			return control.reduce((value, ctrl, id) => {
				const mod = ((id + 1) === control.length) ? 1 : -1
				const code = window.KEYCODES[ctrl]
				return value + (pads[code] ? 1 : 0) * mod
			}, 0)
		}

		// Try to find gamepad, and gracefully return 0 if can't find
		const gamepad = (pads || this.gamepads)[id]
		if (!gamepad) return 0

		let total_value = 0
		let value
		control.forEach((ctrl, id) => {
			switch (ctrl[0]) {
				case "axes":
					value = gamepad.axes[ctrl[1]]
					if (Math.abs(value) < deadzone) { value = 0 }
					break

				case "buttons":
					value = gamepad.buttons[ctrl[1]].value
					if (value < deadzone) { value = 0 }
					break
			}

			const mod = (id + 1) === control.length
			total_value += value * (mod ? 1 : -1)
		})

		return Math.min(Math.max(total_value, -1), 1)
	}

	/**
	 * Gets a control
	 * @param {Number} id ID of control trying to retrieve
	 * @returns {HumanInterface} Interface relating to given ID
	 */
	getControl (id) {
		return this.list[id]
	}

	/**
	 * Retrieves all gamepads from the navigator
	 * @returns {Array} Array of all connected gamepads
	 */
	getGamepads () {
		return window.navigator.getGamepads()
	}

	/**
	 * Whether or not to show player count.
	 * @returns {Boolean} To show or not to show
	 */
	isPlayerCountVisible () {
		return this.show_player_count
	}

	/**
	 * Registers a key as being pressed
	 * @param {Object} evt Event
	 */
	onKeyDown (evt) {
		this.keys[evt.keyCode] = true
	}

	/**
	 * Registers a key as not being pressed
	 * @param {Object} evt Event
	 */
	onKeyUp (evt) {
		this.keys[evt.keyCode] = false
	}

	/**
	 * Check if control was pressed
	 * @param {String} id ID of control to check
	 * @param {Array} control Controls to check
	 * @param {Number} [mod] Mod to control
	 * @returns {Boolean} Whether or not control was pressed
	 */
	pressed (id, control, mod = 1) {
		let current, last

		if (id === "key") {
			current = this.keys
			last = this.last_keys
		} else {
			current = this.gamepad
			last = this.last_gamepads
		}

		const ctrl = this.get(id, control, current) * mod
		const last_ctrl = this.get(id, control, last) * mod

		return (ctrl >= this.deadzone && last_ctrl < this.deadzone)
	}

	/**
	 * Check if control was released
	 * @param {String} id ID of control to check
	 * @param {Array} control Controls to check
	 * @param {Number} [mod] Mod to control
	 * @returns {Boolean} Whether or not control was released
	 */
	released (id, control, mod = 1) {
		let current, last

		if (id === "key") {
			current = this.keys
			last = this.last_keys
		} else {
			current = this.gamepad
			last = this.last_gamepads
		}

		const ctrl = this.get(id, control, current) * mod
		const last_ctrl = this.get(id, control, last) * mod

		return (last_ctrl >= this.deadzone && ctrl < this.deadzone)
	}

	/**
	 * Sets the list of HumanInterfaces
	 * @param {HumanInterface[]} list List of all interfaces
	 */
	setList (list) {
		this.list = list
	}

	/**
	 * Sets up global listeners
	 */
	setupListeners () {
		window.onkeydown = (evt) => {
			this.onKeyDown(evt)

			if (evt.keyCode === 123 && !window.IS_XBOX) {
				window.remote.BrowserWindow.getAllWindows()[0].toggleDevTools()
			}
			if (evt.keyCode === 122 && !window.IS_XBOX) {
				remote.getCurrentWindow().setFullScreen(true)
			}

			if (evt.keyCode === 116) {
				document.location.reload()
			}
		}
		window.onkeyup = this.onKeyUp.bind(this)
	}

	/**
	 * Update all inputs
	 */
	update () {
		this.last_gamepads = {}

		for (let id = 0; id < MAX_INTERFACES; id++) {
			const gamepad = this.gamepads[id]

			if (gamepad) {
				this.last_gamepads[id] = {
					axes: gamepad.axes.map((axis) => {
						return axis
					}),
					buttons: gamepad.buttons.map((button) => {
						return {
							pressed: button.pressed,
							value: button.value,
						}
					}),
				}
			}
		}

		this.gamepads = this.getGamepads()
		Object.keys(this.keys).forEach((key) => {
			this.last_keys[key] = Boolean(this.keys[key])
		})

		this.all_inputs.forEach((input) => {
			if (this.list.find((used_item) => {
				return used_item.type === input.type &&
					used_item.number === input.number
			})) return

			if (input.pressed(CONTROLS.MENU_ACCEPT) && this.list.length < 4 && this.show_player_count) {
				this.list.push(input)
			}
		})
	}
}
