/**
 * State manager
 */

import FirstSetup from "../states/FirstSetup.js"
import LocalGame from "../states/LocalGame.js"
import MainMenu from "../states/MainMenu.js"
import OnlineGame from "../states/OnlineGame.js"
import PressStart from "../states/PressStart.js"
import ResultsScreen from "../states/ResultsScreen.js"
import SplashScreens from "../states/SplashScreens.js"

export const STATES = {
	FirstSetup,
	LocalGame,
	MainMenu,
	OnlineGame,
	PressStart,
	ResultsScreen,
	SplashScreens,
}

/**
 * State manager
 * @module StateManager
 */
export default class StateManager {
	/**
	 * @constructor
	 */
	constructor () {
		window.States = STATES
	}

	/**
	 * Changes to a given state
	 * @param {Object} State State to change to
	 * @param {Object} [opts] Options to pass to state
	 */
	change (State, opts = []) {
		if (this.state) {
			this.state.kill()
		}
		this.state = new State(...opts)
	}

	/**
	 * Updates the current state
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.state.update(dt)
	}
}
