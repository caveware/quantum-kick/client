// Libraries
import Vue from "vue"
import VueRouter from "vue-router"

// Routes
import * as ROUTE from "../constants/routes.js"

// Route layouts
import Blank from "../ui/layouts/Blank.vue"
import CarCustomisation from "../ui/layouts/CarCustomisation.vue"
import Error from "../ui/layouts/Error.vue"
import FirstSetup from "../ui/layouts/FirstSetup.vue"
import Gameplay from "../ui/layouts/Gameplay.vue"
import Lobby from "../ui/layouts/Lobby.vue"
import LobbyList from "../ui/layouts/LobbyList.vue"
import LocalLobby from "../ui/layouts/LocalLobby.vue"
import MainMenu from "../ui/layouts/MainMenu.vue"
import OptionsMenu from "../ui/layouts/OptionsMenu.vue"
import PressStart from "../ui/layouts/PressStart.vue"
import ResultsLocal from "../ui/layouts/ResultsLocal.vue"
import TeamSelection from "../ui/layouts/TeamSelection.vue"
import UI from "../ui/UI.vue"

// RTX route layouts
import RTXControls from "../ui/layouts/RTXControls.vue"
import RTXLobby from "../ui/layouts/RTXLobby.vue"
import RTXSplash from "../ui/layouts/RTXSplash.vue"

/**
 * UI Manager
 * @module UIManager
 */
export default class UIManager {
	/**
	 * @constructor
	 */
	constructor () {
		Vue.use(VueRouter)

		this.setupRouter()
		this.setupUI()
	}

	/**
	 * Emits an action to the UI
	 * @param {String} action Action to execute
	 * @param {Object} [options] Options to pass with action
	 */
	emit (action, options = {}) {
		this.UI.$emit(action, options)
	}

	/**
	 * Stop listening to an event
	 * @param {String} action Action to stop listening to
	 * @param {Function} fn Function to stop listening to
	 */
	off (action, fn) {
		this.UI.$off(action, fn)
	}

	/**
	 * Start listening to an event
	 * @param {String} action Action to listen to
	 * @param {Function} fn Function to perform on event
	 */
	on (action, fn) {
		this.UI.$on(action, fn)
	}

	/**
	 * Sets up the router for the UI
	 */
	setupRouter () {
		const interpretQueries = (route) => {
			const queries = {}

			Object.keys(route.query).forEach((key) => {
				queries[key] = decodeURIComponent(route.query[key])
			})

			return queries
		}

		this.router = new VueRouter({
			routes: [{
				component: Blank,
				path: ROUTE.BLANK,
			}, {
				component: CarCustomisation,
				path: ROUTE.CAR_CUSTOMISATION,
			}, {
				component: Error,
				path: ROUTE.ERROR,
				props: interpretQueries,
			}, {
				component: Gameplay,
				path: ROUTE.GAMEPLAY,
			}, {
				component: Lobby,
				path: ROUTE.JOIN_LOBBY,
				props: interpretQueries,
			}, {
				component: FirstSetup,
				path: ROUTE.FIRST_SETUP,
			}, {
				component: LobbyList,
				path: ROUTE.LOBBY_LIST,
			}, {
				component: LocalLobby,
				path: ROUTE.LOCAL_LOBBY,
			}, {
				component: MainMenu,
				path: ROUTE.MAIN_MENU,
			}, {
				component: OptionsMenu,
				path: ROUTE.OPTIONS_MENU,
			}, {
				component: PressStart,
				path: ROUTE.PRESS_START,
			}, {
				component: RTXControls,
				path: ROUTE.RTX_CONTROLS,
			}, {
				component: RTXLobby,
				path: ROUTE.RTX_LOBBY,
			}, {
				component: RTXSplash,
				path: ROUTE.RTX_SPLASH,
			}, {
				component: ResultsLocal,
				path: ROUTE.RESULTS_LOCAL,
				props: interpretQueries,
			}, {
				component: TeamSelection,
				path: ROUTE.TEAM_SELECTION,
				props: interpretQueries,
			}],
		})
	}

	/**
	 * Sets up the UI
	 */
	setupUI () {
		this.UI = new Vue({
			/**
			 * On creation of UI
			 */
			created: function () {
				this.$router.replace(ROUTE.BLANK)

				this.$on("CHANGE_ROUTE", function (route) {
					// Interpret first part of route, before queries
					switch (route.split("?")[0]) {
						case ROUTE.GAMEPLAY:
						case ROUTE.RESULTS_LOCAL:
						case ROUTE.RTX_LOBBY:
						case ROUTE.RTX_SPLASH:
						case ROUTE.TEAM_SELECTION:
							Input.show_player_count = true
							break
						default:
							Input.show_player_count = false
					}

					this.$router.push(route)
				})

				this.$on("CLEAR_UI", function () {
					this.$router.replace(ROUTE.BLANK)
				})

				this.$on("CRITICAL_NETWORK_ERROR", function (error) {
					this.$router.push(`${ROUTE.ERROR}?text=${encodeURIComponent(error)}`)
				})

				this.$on("JOIN_LOBBY", function (data) {
					const queries = Object.keys(data).map((k) => {
						return `${k}=${encodeURIComponent(data[k])}`
					}).join("&")

					this.$router.push(`/join-lobby?${queries}`)
				})

				this.$on("GO_BACK", function (route) {
					this.$router.go(-1)
					this.$emit("PLAY_BACK_SOUND")
				})

				this.$on("PLAY_BACK_SOUND", () => {
					Sound.playSFX("menu_item_back")
				})
				this.$on("PLAY_CLICK_SOUND", () => {
					Sound.playSFX("menu_item_click")
				})
				this.$on("PLAY_HOVER_SOUND", () => {
					Sound.playSFX("menu_item_hover")
				})

				this.$on("SHOW_TEAM_SELECTION", function (opts) {
					Input.show_player_count = false
					this.$router.push(`${ROUTE.TEAM_SELECTION}?data=${encodeURIComponent(opts)}`)
				})
			},
			data: () => {
				return {
					value: 0,
				}
			},
			el: "#ui",
			render: h => h(UI),
			router: this.router,
		})
	}
}
