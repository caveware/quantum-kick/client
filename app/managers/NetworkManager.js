// Libraries
import { Client } from "colyseus.js"

// Constants
import * as SERVER_DETAILS from "../../common/constants/ServerDetails.js"

/**
 * Network Manager
 * @module NetworkManager
 * @description Handles networking between server and client
 */
export default class NetworkManager extends Client {
	/**
	 * @constructor
	 * @param {String} ip IP to connect to
	 * @param {Object} [callbacks] Object of callbacks
	 */
	constructor (ip, callbacks = {}) {
		// Create networking client
		super(ip || SERVER_DETAILS.DEVELOPMENT)

		this.onOpen.add(this.onOpenCallback.bind(this))

		// Set up callbacks
		this.setCallbacks(callbacks)
	}

	/**
	 * Join a room
	 * @param {String} id ID of room to join
	 */
	joinRoom (id) {
		// If currently in a room, leave it
		if (this.room) this.leaveRoom()

		// Join room
		const name = Settings.get("NAME")
		this.room = this.join(id, { name })
		this.room_name = id

		// Set up callbacks
		this.room.onData.add(this.onData.bind(this))
		this.onError.add(this.onErrorThrow.bind(this))
		this.room.onJoin.add(this.onJoin.bind(this))
		this.room.onUpdate.add(this.onUpdate.bind(this))
	}

	/**
	 * Leaves the current room
	 * @returns {Boolean} Whether or not room was left
	 */
	leaveRoom () {
		// If no room exists, report that we did not leave a room
		if (!this.room) return false

		// Otherwise, leave the room and report that we did
		this.room.leave()
		this.room = undefined
		this.room_name = undefined
		return true
	}

	/**
	 * Sends an action to the server
	 * @param {String} [action] Action to send
	 * @param {Object} [body] Data to send with action
	 */
	sendAction (action = "", body = {}) {
		this.room.send(Object.assign({ action }, body))
	}

	/**
	 * Sets the callbacks for onData, onJoin, onUpdate
	 * @param {Object} [callbacks] List of callbacks
	 */
	setCallbacks (callbacks = {}) {
		this.callbacks = callbacks
	}

	/**
	 * INTERNAL CALLBACKS
	 */

	/**
	 * Callback for onData
	 * @param {Object} data Recieved data
	 */
	onData (data) {
		if (this.callbacks.onData) this.callbacks.onData(data)
	}

	/**
	 * Callback for errors
	 * @param {String} data Error message
	 */
	onErrorThrow (data) {
		if (!data) data = "Connection lost"

		if (this.callbacks.onError) {
			this.callbacks.onError(data)
		}
		this.connection.close()
		delete this // nephew
		window.Networker = undefined
		UI.emit("CRITICAL_NETWORK_ERROR", data)
	}

	/**
	 * Callback for onJoin
	 * @param {Object} data Recieved data
	 */
	onJoin (data) {
		if (this.callbacks.onJoin) this.callbacks.onJoin(data)
	}

	/**
	 * Callback for onOpen
	 * @param {Object} data Recieved data
	 */
	onOpenCallback (data) {
		if (this.callbacks.onOpen) this.callbacks.onOpen(data)
	}

	/**
	 * Callback for onUpdate
	 * @param {Object} data Recieved data
	 */
	onUpdate (data) {
		if (this.callbacks.onUpdate) this.callbacks.onUpdate(data)
	}
}
