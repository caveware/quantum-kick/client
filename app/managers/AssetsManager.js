// Constants
import { MODEL_PATH } from "../constants/paths.js"

/**
 * @author Josef Frank
 * @class AssetsManager
 * @desc Manager to simplify assets loading
 */
export default class AssetsManager extends BABYLON.AssetsManager {
	/**
	 * @constructor
	 * @param {BABYLON.Scene} scene Scene to load assets for
	 * @param {Function} [callback] Function to call after loader finished
	 */
	constructor (scene, callback) {
		super(scene)

		// Add callback
		this.onFinish = callback

		// Hide loading screen
		this.useDefaultLoadingScreen = false
	}

	/**
	 * Loads a mesh with preconfigured data
	 * @param {String} id ID of mesh task
	 * @param {String} path Path to load
	 * @param {Function} [callback] Callback after mesh loaded
	 * @returns {*} Task to load mesh
	 */
	addMeshTask (id, path, callback) {
		// Determine file names / pathing
		const fragments = path.split("/")
		const file = fragments.splice(fragments.length - 1)[0]
		const dir = `${MODEL_PATH + fragments.join("/")}/`

		// Create task
		const task = super.addMeshTask(id, "", dir, file)

		// Add callback
		task.onSuccess = callback

		return task
	}
}
