// Libraries
import { Howl } from "howler"

// Constants
import { BGM, SFX } from "../constants/sounds.js"

/**
 * Sound Manager
 * @module SoundManager
 */
export default class SoundManager {
	/**
	 * @constructor
	 */
	constructor () {
		this.bgm = []
		this.sfx = []

		// Default volumes
		this.bgm_volume = 1
		this.sfx_volume = 1

		// Create all background music
		Object.keys(BGM).forEach((key) => {
			this.createBGM(key, BGM[key])
		})

		// Create all sound effects
		Object.keys(SFX).forEach((key) => {
			this.createSFX(key, SFX[key])
		})
	}

	/**
	 * Create a new BGM sound
	 * @param {String} id ID of new BGM sound
	 * @param {String} path Path at which the sound resides
	 */
	createBGM (id, path) {
		const file = `./assets/sounds/${path}`
		this.bgm[id] = new Howl({
			loop: true,
			src: [file],
		})
	}

	/**
	 * Create a new SFX sound
	 * @param {String} id ID of new SFX sound
	 * @param {String} path Path at which the sound resides
	 */
	createSFX (id, path) {
		const file = `./assets/sounds/${path}`
		this.sfx[id] = new Howl({
			src: [file],
		})
	}

	/**
	 * Play BGM sound
	 * @param {String} id ID of BGM sound to play
	 */
	playBGM (id) {
		if (!this.current_bgm) {
			this.bgm[id].volume(this.bgm_volume)
			this.bgm[id].play()
		} else if (this.current_bgm !== id) {
			const FADE_TIME = 500
			const old_bgm = this.current_bgm
			this.bgm[old_bgm]
				.fade(this.bgm_volume, 0, FADE_TIME)
				.once("fade", () => { this.bgm[old_bgm].stop() })
			this.bgm[id]
				.fade(0, this.bgm_volume, FADE_TIME)
				.play()
		}

		this.current_bgm = id
	}

	/**
	 * Play SFX sound
	 * @param {String} id ID of SFX sound to play
	 */
	playSFX (id) {
		this.sfx[id].play()
	}

	/**
	 * Set BGM sound volume
	 * @param {Number} [volume] Level to set volume to (0-1)
	 */
	updateBGMVolume (volume = 1) {
		if (this.current_bgm) {
			this.bgm[this.current_bgm].volume(volume)
		}

		this.bgm_volume = volume
	}

	/**
	 * Set SFX sound volume
	 * @param {Number} [volume] Level to set volume to (0-1)
	 */
	updateSFXVolume (volume = 1) {
		Object.keys(this.sfx).forEach((key) => {
			this.sfx[key].volume(volume)
		})

		this.sfx_volume = volume
	}
}
