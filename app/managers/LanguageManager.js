export const LANGUAGES = {
	de: require("../text/de.json"),
	en: require("../text/en.json"),
	es: require("../text/es.json"),
	fr: require("../text/fr.json"),
	it: require("../text/it.json"),
	jp: require("../text/jp.json"),
	ko: require("../text/ko.json"),
	zh: require("../text/zh.json"),
}
export const SUPPORTED_LANGUAGES = Object.keys(LANGUAGES)

/**
 * Language Manager
 * @module LanguageManager
 */
export default class LanguageManager {
	/**
	 * @constructor
	 * @param {String} [language] Default language
	 */
	constructor (language = "en") {
		this.language = language
	}

	/**
	 * Return ID of current language
	 * @returns {String} ID of current language
	 */
	get language () {
		return this._language_id
	}

	/**
	 * Sets the current language
	 * @param {String} language_id ID of language
	 */
	set language (language_id) {
		this._language_id = language_id
		this._language = LANGUAGES[language_id]
		if (window.UI) UI.emit("CHANGE_LANGUAGE")
	}

	/**
	 * Get phrase in current language
	 * @param {String} key Key of phrase to get
	 * @returns {String} Phrase in language
	 */
	get (key) {
		return this._language[key]
	}

	/**
	 * Get phrase in current language in all caps
	 * @param {String} key Key of phrase to get
	 * @returns {String} Phrase in language in all caps
	 */
	getAllCaps (key) {
		return this.get(key).toUpperCase()
	}

	/**
	 * Get phrase in current language
	 * @param {String} key Key of phrase to get
	 * @param {String} [language_id] ID of language to use
	 * @returns {String} Phrase in language
	 */
	getIn (key, language_id = "en") {
		return LANGUAGES[language_id][key]
	}
}
