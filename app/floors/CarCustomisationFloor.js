// Classes
import BlankMesh from "../meshes/BlankMesh"
import CarMesh from "../meshes/CarMesh"

/**
 * @author Josef Frank
 * @class CarCustomisationFloor
 * @desc Shows all cars on the floor.
 */
export default class CarCustomisationFloor extends BlankMesh {
	/**
	 * @constructor
	 * @param {BABYLON.Scene} scene Scene to add to
	 * @param {Number} player_count Number of players to show
	 */
	constructor (scene) {
		super("CAR_CONFIG_FLOOR", scene)

		// Create car meshes
		this._config = Settings.get("CAR_CONFIGURATION")
		this.meshes = this._config.map((config, key) => {
			const mesh = new CarMesh(`CAR_MESH_${key}`, scene, config)
			mesh.setParent(this)
			return mesh
		})

		// Internalise scene
		this._scene = scene

		// Where car models will be placed at certain numbers of players
		this._placements = [
			[ { x: 0, z: 0 } ],
			[ { x: 2, z: -2 }, { x: -2, z: 2 } ],
			[ { x: 0, z: -3.5 }, { x: -3.5, z: 0 }, { x: 3.5, z: 3.5 } ],
			[ { x: 0, z: -3.5 }, { x: -3.5, z: 0 }, { x: 3.5, z: 0 }, { x: 0, z: 3.5 } ],
		]

		// Set the player count
		this.setPlayerCount(Input.list.length)
	}

	/**
	 * Disposes of this floor.
	 */
	dispose () {
		this.meshes.forEach((mesh) => {
			this._scene.removeMesh(mesh)
			mesh.dispose()
		})

		super.dispose()
	}

	/**
	 * Sets the car configuration.
	 * @param {Object[]} config Car configuration
	 */
	setConfiguration (config) {
		this._config = config

		config.map((data, key) => {
			const mesh = this.meshes[key]
			mesh.body = data.body
			mesh.wheel = data.wheel
		})
	}

	/**
	 * Sets the number of players
	 * @param {Number} count Count of how many players there are
	 */
	setPlayerCount (count) {
		this._count = count

		const positions = this._placements[count - 1]

		this.meshes.forEach((mesh, key) => {
			mesh.hidden = (key >= count)
			if (!mesh.hidden) {
				const position = positions[key]
				mesh.position.x = position.x
				mesh.position.z = position.z
			}
		})
	}

	/**
	 * Update the customisation floor.
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		if (Input.list.length !== this._count) {
			this.setPlayerCount(Input.list.length)
		}

		if (this.config !== Settings.get("CAR_CONFIGURATION")) {
			this.setConfiguration(Settings.get("CAR_CONFIGURATION"))
		}
	}
}
