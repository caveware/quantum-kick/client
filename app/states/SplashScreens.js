// Libraries
import TWEEN from "@tweenjs/tween.js"

// Constants
import { UI_HEIGHT } from "../constants/ui.js"
const ZERO = BABYLON.Vector3.Zero()

/**
 * Splash screen
 * @module SplashScreens
 */
export default class SplashScreens {
	/**
	 * @constructor
	*/
	constructor () {
		// Create input interfaces
		this.human_interfaces = {
			0: Input.createHumanInterface("pad", 0),
			key: Input.createHumanInterface("key"),
		}

		// Create the scene
		this.scene = new BABYLON.Scene(Engine)
		this.scene.clearColor = new BABYLON.Color3(0, 0, 0)

		Sound.playBGM("menu")

		// Set up the camera
		this.camera = new BABYLON.FreeCamera("menu_camera", ZERO, this.scene)
		this.camera.setTarget(ZERO)

		// Set the UI
		this.UI = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI")
		this.UI.idealHeight = UI_HEIGHT

		// Caveware Digital splash
		this.cd = new BABYLON.GUI.Image("spl_cd", "./assets/gui/splashes/cd.png")
		this.cd.alpha = 0
		this.cd.autoScale = true
		this.UI.addControl(this.cd)

		// Technologies splash
		this.tech = new BABYLON.GUI.Image("spl_tech", "./assets/gui/splashes/tech.png")
		this.tech.alpha = 0
		this.tech.autoScale = true
		this.UI.addControl(this.tech)

		const techFadeOutTween = new TWEEN.Tween(this.tech)
			.to({alpha: 0}, 500)
			.delay(3000)
			.onComplete(() => {
				StateManager.change(States.PressStart)
			})

		const techFadeInTween = new TWEEN.Tween(this.tech)
			.to({alpha: 1}, 500)
			.chain(techFadeOutTween)

		const logoFadeOutTween = new TWEEN.Tween(this.cd)
			.to({alpha: 0}, 500)
			.chain(techFadeInTween)
			.delay(1000)

		new TWEEN.Tween(this.cd)
			.to({alpha: 1}, 500)
			.chain(logoFadeOutTween)
			.start()
	}

	/**
	 * Performed to kill the state
	 */
	kill () {

	}

	/**
	 * Updates the state
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.scene.render()
	}
}
