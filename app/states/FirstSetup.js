/**
 * What to show upon first startup
 * @module FirstSetup
 */
export default class FirstSetup {
	/**
	 * @constructor
	 */
	constructor () {
		UI.emit("CHANGE_ROUTE", "/first-setup")

		UI.on("SELECT_FIRST_SETUP", (id) => {
			switch (id) {
				case 2:
					Settings.set("IS_CONFIGURED", true)
					StateManager.change(States.MainMenu)
					break
			}
		})
	}

	/**
	 * Performed upon setup.
	 */
	onSetupComplete () {
		StateManager.change(States.MainMenu)
	}

	/**
	 * Update state
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		const input = Input.getControl(0)

		// Interpret controls
		if (input.pressed(CONTROLS.LX, -1)) {
			UI.emit("PRESS_LEFT")
		}
		if (input.pressed(CONTROLS.LX, 1)) {
			UI.emit("PRESS_RIGHT")
		}
		if (input.pressed(CONTROLS.LY, -1)) {
			UI.emit("PRESS_UP")
		}
		if (input.pressed(CONTROLS.LY, 1)) {
			UI.emit("PRESS_DOWN")
		}
		if (input.pressed(CONTROLS.MENU_ACCEPT)) {
			UI.emit("PRESS_ENTER")
		}
		if (input.pressed(CONTROLS.MENU_DECLINE)) {
			UI.emit("PRESS_BACK")
		}
	}

	/**
	 * Kill the state
	 */
	kill () {
		UI.off("SELECT_FIRST_SETUP")
	}
}
