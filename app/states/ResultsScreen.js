// Libraries
import TWEEN from "@tweenjs/tween.js"

// Constants
import { UI_HEIGHT } from "../constants/ui.js"
const ZERO = BABYLON.Vector3.Zero()

/**
 * Results Screen
 * @module ResultsScreen
 */
export default class ResultsScreen {
	/**
	 * @constructor
	 * @param {Object} scores Scores for end of game
	 * @param {String} map Map that the game was played on
	 * @param {Array} players All players that played
	 */
	constructor (scores, map, players) {
		this.players = players

		// Create input interfaces
		this.human_interfaces = {
			0: Input.createHumanInterface("pad", 0),
			1: Input.createHumanInterface("pad", 1),
			2: Input.createHumanInterface("pad", 2),
			3: Input.createHumanInterface("pad", 3),
			key: Input.createHumanInterface("key"),
		}

		// Internal values
		this.fade = 0
		this.map = map
		this.selected_button = 0

		// Create the scene
		this.scene = new BABYLON.Scene(Engine)
		this.scene.clearColor = new BABYLON.Color3(.2, .2, .2)

		// Set up the camera
		this.camera = new BABYLON.FreeCamera("results_camera", ZERO, this.scene)
		this.camera.setTarget(ZERO)

		// Set the UI
		this.UI = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI")
		this.UI.idealHeight = UI_HEIGHT

		// Create buttons
		this.buttons = []
		let pa = this.createButton("pa", Language.getAllCaps("PLAY_AGAIN"), "120px", "200px")
		let mm = this.createButton("mm", Language.getAllCaps("MAIN_MENU"), "80px", "300px")

		pa.onPointerMoveObservable.add(() => { this.setSelectedButton(0) })
		mm.onPointerMoveObservable.add(() => { this.setSelectedButton(1) })

		pa.onPointerUpObservable.add(this.onPlayAgain.bind(this))
		mm.onPointerUpObservable.add(this.onMainMenu.bind(this))

		// Show scores
		let winner = "DRAW"
		if (scores.RED > scores.BLUE) {
			winner = "RED"
		} else if (scores.BLUE > scores.RED) {
			winner = "BLUE"
		}
		this.scores = new BABYLON.GUI.TextBlock()
		this.scores.text = `
			${scores.RED} BLOCKS FOR RED,
			${scores.BLUE} BLOCKS FOR BLUE,
			${winner} WINS
		`
		this.scores.fontSize = 24
		this.UI.addControl(this.scores)

		// Load all assets
		const loader = new BABYLON.AssetsManager(this.scene)
		loader.useDefaultLoadingScreen = false
		this.sounds = {}
		const clickTask = loader.addBinaryFileTask("click task", "./assets/sounds/click.ogg")
		clickTask.onSuccess = (task) => {
			this.sounds.click = new BABYLON.Sound("click", task.data, this.scene)
		}
		loader.onFinish = () => {
			this.loaded = true

			// Tween in game
			new TWEEN.Tween(this)
				.to({ fade: 1 }, 500)
				.easing(TWEEN.Easing.Quadratic.Out)
				.start()
		}
		loader.load()
	}

	/**
	 * Creates a new button
	 * @param {String} id Unique identifier of button
	 * @param {String} text Text to show on button
	 * @param {String} [left] Left position of button
	 * @param {String} [top] Top position of button
	 * @returns {BABYLON.GUI.Button} Newly created button
	 */
	createButton (id, text, left = "0px", top = "0px") {
		// Create button
		const btn = new BABYLON.GUI.Button(`btn_${id}`)
		btn.thickness = 0
		btn.left = left
		btn.top = top
		btn.width = "640px"
		btn.height = "64px"
		btn.pointerDownAnimation = () => {}
		btn.pointerEnterAnimation = () => {}
		btn.pointerOutAnimation = () => {}
		btn.pointerUpAnimation = () => {}

		// Create image
		let img = new BABYLON.GUI.Image(`img_${id}`, "./assets/gui/demo_button.png")
		btn.addControl(img)

		// Create text
		let txt = new BABYLON.GUI.TextBlock()
		txt.fontSize = "24px"
		txt.text = text
		btn.addControl(txt)

		// Add button to UI
		this.UI.addControl(btn)
		this.buttons.push(btn)

		return btn
	}

	/**
	 * Fade out the results screen
	 * @param {Function} callback Function to call after fadeout
	 */
	fadeOut (callback) {
		if (this.fade < 1) return

		this.sounds.click.play()
		const tween = new TWEEN.Tween(this)
		tween.to({ fade: 0 }, 500)
		tween.onComplete(callback)
		tween.start()
	}

	/**
	 * Performed when results screen is destroyed
	 */
	kill () {
		this.UI.dispose()

		// Kill camera
		this.camera.dispose()

		// Kill the scene
		this.scene.dispose()
	}

	/**
	 * Takes the user to the main menu
	 */
	onMainMenu () {
		this.fadeOut(() => {
			StateManager.change(States.MainMenu)
		})
	}

	/**
	 * Plays the game again with same rules
	 */
	onPlayAgain () {
		this.fadeOut(() => {
			StateManager.change(States.LocalGame, [{
				map: this.map,
				players: this.players,
			}])
		})
	}

	/**
	 * Sets the selected button
	 * @param {Number} id ID of button to select
	 */
	setSelectedButton (id) {
		if (this.fade < 1) return
		while (id < 0) {
			id += this.buttons.length
		}
		this.selected_button = id % this.buttons.length
	}

	/**
	 * Updates the results screen state
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		// If not loaded, do not allow any update functionality
		if (!this.loaded) return

		// Updates buttons
		this.buttons.forEach((button, id) => {
			// Set alpha of button
			const mod = (id === this.selected_button) ? 1 : .5
			button.alpha = this.fade * mod

			// Set position of button
			const x = (400 - 100 * this.fade - 40 * id)
			button.left = `${x}px`
		})

		// Render the scene
		this.scene.render()

		// Do not allow scene to be controlled if have not faded in
		if (this.fade < 1) return

		// Interpret controls
		Object.values(this.human_interfaces).forEach((hi) => {
			if (hi.pressed(CONTROLS.LY, -1)) {
				this.setSelectedButton(this.selected_button - 1)
			}
			if (hi.pressed(CONTROLS.LY, 1)) {
				this.setSelectedButton(this.selected_button + 1)
			}
			if (hi.pressed(CONTROLS.MENU_ACCEPT)) {
				this.buttons[this.selected_button].onPointerUpObservable.notifyObservers()
			}
		})
	}
}
