// Classes
import GameController from "../../common/controllers/GameController.js"
import GameRenderer from "../renderers/GameRenderer.js"

// Constants
import { GAMEPLAY, RESULTS_LOCAL } from "../constants/routes.js"
import { STATE_NOTHING } from "../constants/tower.js"
import {
	EVENT_GP_UPDATE_PLAYERS,
	EVENT_GP_UPDATE_TIMER,
} from "../constants/ui.js"

// States
const GAMEPLAY_STATE = "GAMEPLAY_STATE"

/**
 * Local game state
 * @module LocalGame
 */
export default class LocalGame {
	/**
	 * @constructor
	 * @param {Object} opts Options for local game
	 */
	constructor (opts) {
		// Default state
		// this.state = TEAM_SELECTION_STATE
		// UI.emit("SHOW_TEAM_SELECTION", JSON.stringify(opts.players))

		// this.sg = this.startGame.bind(this)

		// UI.on("SELECTED_TEAMS", this.sg)

		// Internalise variables
		this.opts = opts

		this.state = GAMEPLAY_STATE
		this.startGame(opts.players.map((player) => {
			return {
				control: player.control,
				side: player.alignment,
				ultimates: player.ultimates,
			}
		}))
	}

	/**
	 * Kill the local game
	 */
	kill () {
		this.controller.dispose()
		this.renderer.dispose()
		UI.off("SELECTED_TEAMS", this.sg)
	}

	/**
	 * Performed upon completion of the game
	 */
	onComplete () {
		Sound.playBGM("result")
		Sound.playSFX("game_over")
		const queries = Object.keys(Score.team_scores).map((key) => {
			return `${key}=${this.controller.scores[key]}`
		}).join("&")
		const route = `${RESULTS_LOCAL}?${queries}`
		Tower.setState(STATE_NOTHING, () => {
			StateManager.change(States.MainMenu, [{ route }])
		})
	}

	/**
	 * Progresses to the next state.
	 * @param {Object} data Data for player alliances.
	 */
	startGame (data) {
		let red_left = (this.opts.players.length / 2) - data.filter((update) => {
			return update.side === "red"
		}).length

		// this.opts.players.forEach((player) => {
		// 	if (!player.ai) {
		// 		data.forEach((update) => {
		// 			if (update.control === player.control) {
		// 				player.alignment = update.side.toUpperCase()
		// 			}
		// 		})
		// 	} else {
		// 		if (red_left > 0) {
		// 			red_left--
		// 			player.alignment = "RED"
		// 		} else {
		// 			player.alignment = "BLUE"
		// 		}
		// 	}
		// })

		this.opts.players = this.opts.players.sort((p1, p2) => {
			return p1.alignment < p2.alignment
		})

		const players = []
		for (let x = 0; x <= 1; x++) {
			for (let y = x; y < this.opts.players.length; y += 2) {
				players.push(this.opts.players[y])
			}
		}
		this.opts.players = players

		UI.emit("CHANGE_ROUTE", GAMEPLAY)
		this.state = GAMEPLAY_STATE

		// Create controller
		this.controller = new GameController(Object.assign({
			onTimeUp: () => {
				if (!this.pause) this.onComplete()
				this.pause = true
			},
		}, this.opts))

		// Create renderer
		const data_new = this.controller.data
		this.renderer = new GameRenderer(this.opts, data_new)
		this.countdown = data_new.time_remaining

		Sound.playBGM("game_1")
		Sound.playSFX("announcer_3")
	}

	/**
	 * Updates the local game
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		if (this.state === GAMEPLAY_STATE) {
			if (!this.pause) this.controller.update(dt)
			const data = this.controller.data
			this.renderer.update(dt, data)
			UI.emit(EVENT_GP_UPDATE_PLAYERS, data.players)
			UI.emit(EVENT_GP_UPDATE_TIMER, data.time_remaining)

			if (this.countdown !== undefined) {
				if (data.time_remaining > this.countdown) {
					Sound.playSFX("announcer_go")
					this.countdown = undefined
				} else {
					if (this.countdown > 2 && data.time_remaining <= 2) {
						Sound.playSFX("announcer_2")
					}

					if (this.countdown > 1 && data.time_remaining <= 1) {
						Sound.playSFX("announcer_1")
					}

					this.countdown = data.time_remaining
				}
			}
		}
	}
}
