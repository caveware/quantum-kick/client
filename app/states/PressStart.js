// Constants
import * as ROUTE from "../constants/routes.js"
import * as SETTING from "../constants/settings.js"

/**
 * "Press Start" screen
 * @module PressStart
 */
export default class PressStart {
	/**
	 * @constructor
	 */
	constructor () {
		this.input_k = window.Input.createHumanInterface("key", 0)
		this.input_j = window.Input.createHumanInterface("pad", 0)

		UI.emit("CHANGE_ROUTE", ROUTE.PRESS_START)
	}

	/**
	 * Set the primary interface
	 * @param {HumanInterface} controller Primary interface
	 */
	setPrimaryController (controller) {
		Input.setList([controller])
		// const state = Settings.get(SETTING.IS_CONFIGURED)
		// 	? States.MainMenu
		// 	: States.FirstSetup
		UI.emit("PLAY_CLICK_SOUND")
		Sound.playSFX("press_start")
		StateManager.change(States.MainMenu)
	}

	/**
	 * Update the state
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		if (this.input_k.pressed(CONTROLS.MENU_ACCEPT)) {
			this.setPrimaryController(this.input_k)
		}

		if (this.input_j.pressed(CONTROLS.MENU_ACCEPT)) {
			this.setPrimaryController(this.input_j)
		}
	}

	/**
	 * Performed when state is destroyed
	 */
	kill () { }
}
