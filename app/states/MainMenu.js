// Constants
import * as ROUTE from "../constants/routes.js"
import * as SETTING from "../constants/settings.js"

/**
 * @author Josef Frank
 * @class MainMenu
 * @desc Main menu
 */
export default class MainMenu {
	/**
	 * @constructor
	 * @param {String} [data] Data for the game
	 */
	constructor (data = {}) {		// Set route
		UI.emit("CHANGE_ROUTE", data.route || ROUTE.RTX_LOBBY)

		// Set up UI callbacks
		UI.on("SELECT_MAIN_MENU", this._onSelectMainMenuOption)
		UI.on("SELECT_OPTIONS_MENU", this._onSelectOptionsMenuOption)
	}

	/**
	 * Performed upon selection of main menu option.
	 * @param {Number} id ID of main menu option
	 * @private
	 */
	_onSelectMainMenuOption (id) {
		switch (id) {
			case 0:
				UI.emit("CHANGE_ROUTE", ROUTE.LOBBY_LIST)
				break
			case 1:
				UI.emit("CHANGE_ROUTE", ROUTE.LOCAL_LOBBY)
				break
			case 2:
				UI.emit("CHANGE_ROUTE", ROUTE.CAR_CUSTOMISATION)
				break
			case 3:
				UI.emit("CHANGE_ROUTE", ROUTE.OPTIONS_MENU)
				break
			case 4:
				window.remote.getCurrentWindow().close()
				break
		}
	}

	/**
	 * Performed upon selection of options menu option.
	 * @param {Number} id ID of options menu option
	 */
	_onSelectOptionsMenuOption (id) {
		switch (id) {
			case 4:
				const value = !Settings.get(SETTING.FULLSCREEN)
				Settings.set(SETTING.FULLSCREEN, value)
				break
		}
	}

	/**
	 * Updates the main menu state
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		const input = Input.getControl(0)

		// Interpret controls
		if (input.pressed(CONTROLS.LX, -1)) {
			UI.emit("PRESS_LEFT")
		}
		if (input.pressed(CONTROLS.LX, 1)) {
			UI.emit("PRESS_RIGHT")
		}
		if (input.pressed(CONTROLS.LY, -1)) {
			UI.emit("PRESS_UP")
		}
		if (input.pressed(CONTROLS.LY, 1)) {
			UI.emit("PRESS_DOWN")
		}
		if (input.pressed(CONTROLS.MENU_ACCEPT)) {
			UI.emit("PRESS_ENTER")
		}
		if (input.pressed(CONTROLS.MENU_DECLINE)) {
			UI.emit("PRESS_BACK")
		}
	}

	/**
	 * Destroys the state
	 */
	kill () {
		UI.off("SELECT_MAIN_MENU", this._onSelectMainMenuOption)
		UI.off("SELECT_OPTIONS_MENU", this._onSelectOptionsMenuOption)
	}
}
