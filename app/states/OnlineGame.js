// Classes
import GameController from "../../common/controllers/GameController.js"
import GameRenderer from "../renderers/GameRenderer.js"

// Constants
import * as ACTION from "../../common/constants/Actions.js"
/**
 * Online game state
 * @module OnlineGame
 */
export default class OnlineGame {
	/**
	 * @constructor
	 * @param {Object} opts Options for the online game
	 */
	constructor (opts) {
		window.Networker.setCallbacks({
			onData: (data) => {
				switch (data.action) {
					case ACTION.COMPLETE_GAME:
						StateManager.change(States.ResultsScreen, [
							data.scores,
							this.opts.map,
							this.opts.players,
						])
						break
				}
			},
			onJoin: () => { this.is_connected = true },
			onUpdate: (data) => {
				if (data.blocks && data.blocks.length) {
					this.controller.blocks.forEach((block, key) => {
						block.alignment = data.blocks[key]
					})
				}
				if (data.players && data.players.length) {
					this.controller.players.forEach((player, key) => {
						const p = data.players[key]
						player._phase = p.phase
						player._position.x = p.x
						player._position.z = p.z
						player._rotation.y = p.ry

						if (p.current_block) {
							p.current_block = p.current_block
						}
						if (p.next_block) {
							p.next_block = p.next_block
						}
					})
				}
				this.controller.timer._time = data.time_remaining
			},
		})
		window.Networker.joinRoom(opts.room_id)

		// Set up players
		this.players = opts.players.map((player, key) => {
			const details = Object.assign(player, { control: "-1" })

			if (player.id === window.Networker.id) {
				details.control = "key"
				this.self_key = key
			}

			return details
		})

		// Internalise variables
		this.opts = {
			map: window.Maps[opts.map_name],
			players: this.players,
		}

		// Create controller
		this.controller = new GameController(Object.assign({
			onTimeUp: () => { },
		}, this.opts))

		// Create renderer
		this.renderer = new GameRenderer(this.opts, this.controller.data)

		UI.emit("CLEAR_UI")
		Sound.playBGM("game_1")
	}

	/**
	 * Kill the local game
	 */
	kill () {
		this.controller.dispose()
		this.renderer.dispose()
	}

	/**
	 * Performed upon completion of the game
	 */
	onComplete () {
		StateManager.change(States.ResultsScreen, [
			this.controller.scores,
			this.opts.map,
			this.opts.players,
		])
	}

	/**
	 * Updates the local game
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		// this.controller.update(dt)
		this.renderer.update(dt, this.controller.data)

		if (!this.is_connected) return

		const controller = this.controller.players[this.self_key].controller
		window.Networker.sendAction(ACTION.PLAYER_UPDATE, {
			accel: controller.get(CONTROLS.DRIVE),
			lx: controller.get(CONTROLS.LX),
			ly: controller.get(CONTROLS.LY),
		})
	}
}
