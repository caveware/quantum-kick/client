import { ipcRenderer } from "electron"

let config = {}
let particles

const CANVAS = document.getElementById("game")

// Create engine
const engine = new BABYLON.Engine(CANVAS, true)
window.addEventListener("resize", engine.resize.bind(engine))
engine.enableOfflineSupport = false

// Create scene
const scene = new BABYLON.Scene(engine)

// Create camera
const target = BABYLON.Vector3.Zero()
const camera = new BABYLON.ArcRotateCamera("camera", 1, 1, 50, target, scene)
camera.attachControl(CANVAS, true)

// Create light
const direction = new BABYLON.Vector3(1.5, 2, -1)
const light = new BABYLON.HemisphericLight("light", direction, scene)
light.intensity = .9

/**
 * Function to set up scene
 */
function setupScene () {
	scene.box = BABYLON.MeshBuilder.CreateBox("box", {
		size: config.meshSize,
	}, scene)
	scene.clearColor = new BABYLON.Color3(...config.backgroundColour)
}

/**
 * Function to clear up scene
 */
function clearScene () {
	scene.removeMesh(scene.box)
}

// Set up events
const setError = (text = "") => {
	document.getElementById("ui").innerText = text
}
ipcRenderer.on("config-updated", (event, data) => {
	clearScene()
	config = data
	setupScene()
	setError()
})
ipcRenderer.on("particles-updated", (event, data) => {
	if (particles) {
		particles.stop()
		particles.dispose()
	}

	// Set up emitter
	particles = new BABYLON.ParticleSystem("particles", data.maxParticles, scene, null, data.textureAnimates)
	particles.blendMode = BABYLON.ParticleSystem.BLENDMODE_STANDARD
	particles.disposeOnStop = true
	particles.emitter = scene.box

	// Particle images
	const image = `./src/images/${data.image}`
	particles.particleTexture = new BABYLON.Texture(image, scene, true,
		false, BABYLON.Texture.TRILINEAR_SAMPLINGMODE)
	particles.textureMask = new BABYLON.Color4(...data.textureMask)

	// Animation settings
	particles.startSpriteCellID = data.startSpriteCellID
	particles.endSpriteCellID = data.endSpriteCellID
	particles.spriteCellHeight = data.spriteCellHeight
	particles.spriteCellWidth = data.spriteCellWidth
	particles.spriteCellLoop = data.spriteCellLoop
	particles.spriteCellChangeSpeed = data.spriteCellChangeSpeed

	// Color settings
	particles.color1 = new BABYLON.Color4(...data.color1)
	particles.color2 = new BABYLON.Color4(...data.color2)
	particles.colorDead = new BABYLON.Color4(...data.colorDead)

	// Direction settings
	particles.direction1 = new BABYLON.Vector3(...data.direction1)
	particles.direction2 = new BABYLON.Vector3(...data.direction2)

	// Rotational settings
	particles.maxAngularSpeed = data.maxAngularSpeed
	particles.minAngularSpeed = data.minAngularSpeed

	// Emit settings
	particles.emitRate = data.emitRate
	particles.maxEmitBox = new BABYLON.Vector3(...data.maxEmitBox)
	particles.minEmitBox = new BABYLON.Vector3(...data.minEmitBox)
	particles.maxEmitPower = data.maxEmitPower
	particles.minEmitPower = data.minEmitPower

	// Life time
	particles.maxLifeTime = data.maxLifeTime
	particles.minLifeTime = data.minLifeTime

	// Size
	particles.maxSize = data.maxSize
	particles.minSize = data.minSize

	// General settings
	particles.gravity = new BABYLON.Vector3(...data.gravity)
	particles.manualEmitCount = data.emitCount
	particles.targetStopDuration = data.targetStopDuration
	particles.updateSpeed = data.updateSpeed

	// Start simulation
	particles.start()
	setError()
})
ipcRenderer.on("error", (event, err) => {
	setError(err)
})

// Set up render loop
engine.runRenderLoop(scene.render.bind(scene))
