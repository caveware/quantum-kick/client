module.exports = {
	devServer: {
		contentBase: "./",
	},
	entry: "./play.js",
	module: {
		loaders: [{
			loader: "url-loader",
			test: /\.(png|jpe?g|gif)(\?.*)?$/,
		}, {
			loader: "vue-loader",
			test: /\.vue$/,
		}],
	},
	output: {
		filename: "gamebundle.js",
	},
	target: "electron",
}
