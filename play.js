/**
 * Game will go here
 */

require("./app/bootstrap.js")

// States.SplashScreens
StateManager.change(States.PressStart, [{
	map: window.Maps.default,
	players: [{
		body: "police-car",
		control: "key",
		wheel: "normal",
	}, {
		ai: true,
		body: "police-car",
		wheel: "normal",
	}],
}])

let now = Date.now()

Engine.runRenderLoop(() => {
	// Update the delta time
	const time = Date.now()
	const dt = Math.min(1 / 15, (time - now) / 1000)
	now = time

	// Update managers
	StateManager.update(dt)
	Input.update(dt)
	Tower.render(dt)
	UI.UI.$forceUpdate()

	if (Input.list[0] && Input.list[0].pressed(CONTROLS.DEV_RELOAD)) {
		document.location.reload()
	}

	// Calculate tweens
	window.TWEEN.update()
})
