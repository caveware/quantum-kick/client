const { app, BrowserWindow } = require("electron")
const path = require("path")
const url = require("url")

let win

/**
 * Create the game window
 */
function createWindow () {
	win = new BrowserWindow({
		backgroundColor: "#000",
		frame: false,
		height: 1200,
		resizable: true,
		skipTaskbar: false,
		toolbar: false,
		width: 1920,
	})
	// win.webContents.openDevTools()
	// win.setFullScreen(true)

	win.loadURL(url.format({
		pathname: path.join(__dirname, "index.html"),
		protocol: "file:",
		skipTaskbar: true,
		slashes: true,
		toolbar: false,
	}))
	win.setMenu(null)
	win.setTitle("QUANTUM KICK")
}

app.on("ready", createWindow)
